function A = STCHochbaum(edges, d)
%% Init
%M = dlmread('./datasets/toy.csv', ';');
% M = dlmread('./../datasets/students.csv', ';');
%M = dlmread('./datasets/enron.csv', ';');
%M = dlmread('./datasets/facebookBig.csv', ';');
%M = dlmread('./datasets/twitterBig.csv', ';');
%M = dlmread('./datasets/tumblr.csv', ';');
% M = dlmread('./../datasets/enron.csv', ';');
%edges = M(:, [1 2]);
%edges
%clear M
%d = 2;

Adj = sparse(edges(:,1), edges(:,2), 1, max(max(edges)), max(max(edges)));
%%
%clear edges
Adj = (Adj | Adj');
Adj = Adj-diag(diag(Adj)); % Just to make sure the diagonal elements are zero (no self-loops)

Adj_counter = triu(Adj);
Adj_counter(Adj_counter>0) = 1:sum(sum(Adj_counter));

% clear Adj
% Compute solution

% enumerate triangles
triangleedges = get_triangles(Adj);

[eq_classes, representatives, metha_nodes, mnodes_size] = get_eq_classes(triangleedges, Adj);
%metha_nodes_indexes = find(ismember(representatives, metha_nodes));
%Adj_counter_repr = Adj_counter(representatives, representatives);

Adj_counter_red = logical(Adj_counter(representatives, representatives));
Adj_counter_red = triu(double(Adj_counter_red + Adj_counter_red'));
Adj_red = Adj_counter_red.*(repmat(mnodes_size, length(mnodes_size), 1).*(repmat(mnodes_size, length(mnodes_size), 1)'));
Adj_counter_red(Adj_counter_red > 0) = 1:sum(sum(Adj_counter_red));

me_weight = full(Adj_red(find(Adj_counter_red>0)));
%mn_weight = mnodes_size(ismember(representatives, metha_nodes));
mn_weight = mnodes_size(metha_nodes);
mn_weight = (mn_weight.*(mn_weight-1)./2);

%[Adj_counter, wedges, triangleVs, wedge_edges, tri_only_edges, clique_edges, all_edges] = get_edge_types(Adj_counter_red, representatives);
[wedges, triangleVs] = get_edge_types(Adj_counter_red, representatives);


%%
upper_lim = d + 1;
number_of_levels = upper_lim + 1;

% get types of edges
%[wedge_edges, tri_only_edges, clique_edges] = get_edge_types(Adj_counter, wedges, triangleVs);

% max sum(0.5*x_plus - 0.5*x_minus)
% -x_plus + x_minus >= -1 (wedge edges)
% -x1_plus + (d-1)x2_plus >= -2 (x1 - trianles-only edge; x2 - appears in some wedge)
% x1_minus - (d-1)x2_minus >= -2 (x1 - trianles-only edge; x2 - appears in some wedge)
% trianles-only edges: x_plus = [0, d+1], x_minus = [-d-1, 0]
% other edges: x_plus = [0, 1], x_minus = [-1, 0]

% enumerate variables
num_medges = max(Adj_counter_red(:));
num_mnodes = length(metha_nodes);

wedges_plus = reshape(1:(num_medges)*2, [num_medges, 2]);
counter = max(wedges_plus(:));
wedges_minus = reshape((1:(num_medges)*2) + counter, [num_medges, 2]);
counter =  max(wedges_minus(:));


tredges_plus = reshape((1:(num_mnodes)*(number_of_levels)) + counter, [num_mnodes, number_of_levels]);
counter = max(tredges_plus(:));
tredges_minus = reshape((1:(num_mnodes)*(number_of_levels)) + counter, [num_mnodes, number_of_levels]);
%counter =  max(tredges_minus(:));

counter = max([tredges_minus(:); tredges_plus(:); wedges_minus(:); wedges_plus(:)]);
s_node = counter + 1;
t_node = s_node + 1;

% first set of constraints (from s)
% wedges
meta_edges_s1w = s_node*ones(1, num_medges); % from s
meta_edges_t1w = wedges_plus(:, 2)'; % to all levels except for l_i level
%meta_edges_w1w = 0.5*ones(size(meta_edges_s1w));
meta_edges_w1w = 0.5.*me_weight';

meta_edges_s15w = s_node*ones(1, num_medges*2); % from s
meta_edges_t15w = [wedges_plus(:, 1)', wedges_minus(:, 1)']; % to all l_i level
meta_edges_w15w = [Inf*ones(1, num_medges), Inf*ones(1, num_medges)];
%meta_edges_w15w = [0.5*ones(1, num_medges), 1.0*ones(1, num_medges)];

% triangles
meta_edges_s1tr = s_node*ones(1, num_mnodes*(number_of_levels-1));
tmp = tredges_plus(:,2:end);
meta_edges_t1tr = tmp(:)';
%meta_edges_w1tr = 0.5*ones(size(meta_edges_s1tr));
tmp = 0.5*repmat(mn_weight,1,(number_of_levels-1));
meta_edges_w1tr = tmp(:)';

meta_edges_s15tr = s_node*ones(1, num_mnodes*2);
meta_edges_t15tr = [tredges_plus(:,1)', tredges_minus(:,1)'];
meta_edges_w15tr = [Inf*ones(1, num_mnodes), Inf*(d+1)*ones(1, num_mnodes)];
%meta_edges_w15tr = [0.5*ones(1, num_mnodes), 0.5*(d+2)*ones(1, num_mnodes)];

% second set of constraints (to t)
% wedges
meta_edges_s2w = wedges_minus(:,2)'; % from all levels except for l_i level
meta_edges_t2w = t_node*ones(1, num_medges); % to t
%meta_edges_w2w = 0.5*ones(size(meta_edges_s1w));
meta_edges_w2w = 0.5.*me_weight';

% triangles
tmp = tredges_minus(:,2:end);
meta_edges_s2tr = tmp(:)';
meta_edges_t2tr = t_node*ones(1, num_mnodes*(number_of_levels-1));
%meta_edges_w2tr = 0.5*ones(size(meta_edges_s2tr));
meta_edges_w2tr = 0.5*repmat(mn_weight,1,(number_of_levels-1));

% combine 1 and 2 sets of constraints
meta_edges_c12_s = [meta_edges_s1w, meta_edges_s15w, meta_edges_s1tr, meta_edges_s15tr, meta_edges_s2w, meta_edges_s2tr];
meta_edges_c12_t = [meta_edges_t1w, meta_edges_t15w, meta_edges_t1tr, meta_edges_t15tr, meta_edges_t2w, meta_edges_t2tr];
meta_edges_c12_w = [meta_edges_w1w, meta_edges_w15w, meta_edges_w1tr, meta_edges_w15tr, meta_edges_w2w, meta_edges_w2tr];


%clearvars meta_edges_s1w meta_edges_s15w meta_edges_s1tr meta_edges_s15tr meta_edges_s2w meta_edges_s2tr
%clearvars meta_edges_s1w meta_edges_s15w meta_edges_s1tr meta_edges_s15tr meta_edges_s2w meta_edges_s2tr
%clearvars meta_edges_s1w meta_edges_s15w meta_edges_s1tr meta_edges_s15tr meta_edges_s2w meta_edges_s2tr

% third set of constraints from the next level to the previous
% wedges plus variables 
meta_edges_s3wp = wedges_plus(:,2)';
meta_edges_t3wp = wedges_plus(:,1)';
meta_edges_w3wp = Inf*ones(size(meta_edges_s3wp));

% wedges minus variables 
meta_edges_s3wn = wedges_minus(:,2)';
meta_edges_t3wn = wedges_minus(:,1)';
meta_edges_w3wn = Inf*ones(size(meta_edges_s3wn));

% triangle plus variables
tmp = tredges_plus(:,2:end);
meta_edges_s3trp = tmp(:)';
tmp = tredges_plus(:,1:end-1);
meta_edges_t3trp = tmp(:)';
meta_edges_w3trp = Inf*ones(size(meta_edges_s3trp));

% triangle minus variables 
tmp = tredges_minus(:,2:end);
meta_edges_s3trm = tmp(:)';
tmp = tredges_minus(:,1:end-1);
meta_edges_t3trm = tmp(:)';
meta_edges_w3trm = Inf*ones(size(meta_edges_s3trm));

meta_edges_c3_s = [meta_edges_s3wp, meta_edges_s3wn, meta_edges_s3trp, meta_edges_s3trm];
meta_edges_c3_t = [meta_edges_t3wp, meta_edges_t3wn, meta_edges_t3trp, meta_edges_t3trm];
meta_edges_c3_w = [meta_edges_w3wp, meta_edges_w3wn, meta_edges_w3trp, meta_edges_w3trm];

clearvars meta_edges_s3wp meta_edges_s3wn meta_edges_s3trp meta_edges_s3trm
clearvars meta_edges_t3wp meta_edges_t3wn meta_edges_t3trp meta_edges_t3trm
clearvars meta_edges_w3wp meta_edges_w3wn meta_edges_w3trp meta_edges_w3trm
%%
% 4th set of constraints
% -x_plus + x_minus >= -1 (wedge edges)

meta_edges_s4w = zeros(1, size(wedges,1)*4);
meta_edges_t4w = zeros(1, size(wedges,1)*4);
meta_edges_w4w = zeros(1, size(wedges,1)*4);

for j = 1:size(wedges,1)
    i = wedges(j,:);
    %w1 = find(wedge_edges == i(1));
    %w2 = find(wedge_edges == i(2));
    w1 = i(1);
    w2 = i(2);
    
    meta_edges_t4w(4*(j-1)+1) = wedges_minus(w1,2);
    meta_edges_t4w(4*(j-1)+2) =  wedges_minus(w2,2);
    meta_edges_t4w(4*(j-1)+3) = wedges_minus(w1,1);
    meta_edges_t4w(4*(j-1)+4) =  wedges_minus(w2,1);
    
    meta_edges_s4w(4*(j-1)+1) = wedges_plus(w2,2);
    meta_edges_s4w(4*(j-1)+2) =  wedges_plus(w1,2);
    meta_edges_s4w(4*(j-1)+3) = wedges_plus(w2,1);
    meta_edges_s4w(4*(j-1)+4) =  wedges_plus(w1,1);
    
    meta_edges_w4w(4*(j-1)+1) = Inf;
    meta_edges_w4w(4*(j-1)+2) = Inf;
    meta_edges_w4w(4*(j-1)+3) = Inf;
    meta_edges_w4w(4*(j-1)+4) = Inf; 
end
%%
%tr_indicators = ismember(triangleVs, tri_only_edges);
%indexes = find(sum(tr_indicators(:,[1,2]),2) == 1);

triangleVs = triangleVs(ismember(triangleVs(:,1), metha_nodes), :);

% x1_minus - (d-1)x2_minus >= -2 (x1 - trianles-only edge; x2 - appears in some wedge)
%meta_edges_s5 = zeros(1, size(indexes,1)*2);
%meta_edges_t5 = zeros(1, size(indexes,1)*2);
%meta_edges_w5 = zeros(1, size(indexes,1)*2);

meta_edges_s5 = zeros(1, size(triangleVs,1)*2);
meta_edges_t5 = zeros(1, size(triangleVs,1)*2);
meta_edges_w5 = zeros(1, size(triangleVs,1)*2);

for j = 1:size(triangleVs,1)
    i = triangleVs(j,:);
    %tr_id = find(tr_indicators(i,[1,2]));
    %w_id = find(~tr_indicators(i,[1,2]));
    %tr = find(tri_only_edges == triangleVs(i,tr_id));
    %w = find(wedge_edges == triangleVs(i,w_id));
    tr = i(1);
    w = i(2);
    
    meta_edges_s5(2*j-1) = wedges_minus(w,1);
    meta_edges_w5(2*j-1) = Inf;
    meta_edges_t5(2*j-1) = tredges_minus(tr,1);
    
    meta_edges_s5(2*j) = wedges_minus(w,2);
    meta_edges_w5(2*j) = Inf;
    meta_edges_t5(2*j) = tredges_minus(tr, end-2);
end
%%
% -x1_plus + (d-1)x2_plus >= -2 (x1 - trianles-only edge; x2 - appears in some wedge)
%meta_edges_s6 = zeros(1,length(indexes)*(upper_lim+1));
%meta_edges_t6 = zeros(1,length(indexes)*(upper_lim+1));
%meta_edges_w6 = zeros(1,length(indexes)*(upper_lim+1));

meta_edges_s6 = zeros(1, size(triangleVs,1)*(upper_lim+1));
meta_edges_t6 = zeros(1, size(triangleVs,1)*(upper_lim+1));
meta_edges_w6 = zeros(1, size(triangleVs,1)*(upper_lim+1));

for j = 1:size(triangleVs,1)
    i = triangleVs(j,:);
    tr = i(1);
    w = i(2);
    
    for l=1:number_of_levels
        level = l-1;
       
        meta_edges_s6((j-1)*number_of_levels + l) = tredges_plus(tr, l);
        meta_edges_w6((j-1)*number_of_levels + l) = Inf;
        qp = ceil((level-2)/(d-1));
        if qp >= 0 && qp <= 1
            meta_edges_t6((j-1)*number_of_levels + l) = wedges_plus(w, qp+1);
        else
            meta_edges_t6((j-1)*number_of_levels + l) = tredges_plus(tr, l);
        end
    end
end

meta_edges_c4_s = [meta_edges_s4w, meta_edges_s5, meta_edges_s6];
meta_edges_c4_t = [meta_edges_t4w, meta_edges_t5, meta_edges_t6];
meta_edges_c4_w = [meta_edges_w4w, meta_edges_w5, meta_edges_w6];

%clearvars meta_edges_s4w meta_edges_s5 meta_edges_s6
%clearvars meta_edges_t4w meta_edges_t5 meta_edges_t6
%clearvars meta_edges_w4w meta_edges_w5 meta_edges_w6

% get max flow graph

meta_edges_s = [meta_edges_c12_s, meta_edges_c3_s, meta_edges_c4_s];
meta_edges_t = [meta_edges_c12_t, meta_edges_c3_t, meta_edges_c4_t];
meta_edges_w = [meta_edges_c12_w, meta_edges_c3_w, meta_edges_c4_w];

constraints = (unique([meta_edges_s; meta_edges_t; meta_edges_w]', 'rows', 'stable'))';
G = digraph(constraints(1,:), constraints(2,:), constraints(3,:));
%%
%h = plot_toy_graph(G, wedge_edges, tri_only_edges, wedges_plus, wedges_minus, tredges_plus, tredges_minus, num_mnodes, num_medges, upper_lim, s_node, t_node);
%H = graph(Adj_counter);
%figure(2);
%plot(H, 'EdgeLabel', H.Edges.Weight);


%%solve maxflow
[mf,~,cs,ct] = maxflow(G, s_node, t_node);
%'solution cost', mf
%highlight(h,cs,'NodeColor','g');

solution = zeros(size(Adj));
solution_w = zeros(1, num_medges);

[r,c] = find(Adj_counter_red, 1);

for i=1:length(solution_w)
    range = 0:1;
    level_plus = max(range(ismember(wedges_plus(i,:), cs)));
    range = -1:0;
    level_minus = max(range(ismember(wedges_minus(i,:), cs)));
    solution_w(i) = (level_plus - level_minus)/2;
    [r,c] = find(Adj_counter_red == i);
    for node1 = eq_classes{r}
        for node2 = eq_classes{c}
            solution(node1, node2) = solution_w(i); 
        end
    end
end
%%

solution_tr = zeros(1, num_mnodes); 

for i=1:length(solution_tr)
    range = 0:upper_lim;
    level_plus = max(range(ismember(tredges_plus(i,:), cs)));
    range = -upper_lim:0;
    level_minus = max(range(ismember(tredges_minus(i,:), cs)));
    solution_tr(i) = (level_plus - level_minus)/2;
    c = metha_nodes(i);
    nodes = eq_classes{c};
    for n1 = 1:length(nodes)
        for n2 = (n1+1):length(nodes)
            solution(nodes(n1), nodes(n2)) = solution_tr(i); 
        end
    end
end

A = solution + solution';

end