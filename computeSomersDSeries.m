function sd = computeSomersDSeries(A, edges, v, d)

% CVXPATH = '\\Client\C$\Users\fawadria\matlab\cvx\';
CVXPATH = '/Users/jlijffijt/matlab/cvx/';
gcp('nocreate');
% addpath(CVXPATH, [CVXPATH 'functions\'], [CVXPATH 'lib\']);
addpath(CVXPATH, [CVXPATH 'functions/'], [CVXPATH 'lib/']);

% Init
n = length(d);
sd = zeros(1,n);

for i = 1:n
    % Compute symmetric optimal solution
    [~,x,x_edges] = strong_weak_links_cvx_2var(A,0,d(i),1);
    x = round(x*2)/2;

    % Give index of edges to map to x_edges
    ix = alignEdges(edges,x_edges);

    % Compute Somers' D rank correlation
    sd(i)  = somersD(x(ix),v);
end
end