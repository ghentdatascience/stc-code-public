cvx_solver mosek
% cvx_solver_settings('MSK_IPAR_INTPNT_BASIS','MSK_BI_NEVER') % This setting is used in hopes that a point in the relative interior of the optimal face is found, rather than a vertex of the optimal face
% cvx_solver_settings -clear % This should be called to restore default settings, which ensure that a vertex in the optimal face is found.


%% Toy datasets

% Clique with appending edge

Adj = ones(4,4);
Adj(4,5)=1; Adj(5,4)=1;
Adj = Adj-diag(diag(Adj));

[is,js,~] = find(triu(Adj));
G = graph(is,js,ones(length(is),1));
figure,plot(G);

upper_bounded = 0;
d=1/2;
force_symmetric = 1;

tic;[opt,x,x_edges] = strong_weak_links_cvx(Adj,upper_bounded,d,force_symmetric);toc

% The value of the objective:
sum(x)
% The trivial value of the objective by setting all strengths to medium:
length(is)/2
figure,plot(sort(x))
[x_edges x]



% Clique with missing edge and appending edge

Adj = ones(6,6);
Adj(1,2)=0; Adj(2,1)=0;
Adj(6,7)=1; Adj(7,6)=1;
Adj = Adj-diag(diag(Adj));

[is,js,~] = find(triu(Adj));
G = graph(is,js,ones(length(is),1));
figure,plot(G);

upper_bounded = 0;
d=1/2;
force_symmetric = 1;

tic;[opt,x,x_edges] = strong_weak_links_cvx(Adj,upper_bounded,d,force_symmetric);toc

% The value of the objective:
sum(x)
% The trivial value of the objective by setting all strengths to medium:
length(is)/2
figure,plot(sort(x))
[x_edges x]



% Bow tie with connecting edge

Adj = [ones(3,3) zeros(3,3) ; zeros(3,3) ones(3,3)];
Adj(3,4)=1; Adj(4,3)=1;
Adj = Adj-diag(diag(Adj));

[is,js,~] = find(triu(Adj));
G = graph(is,js,ones(length(is),1));
figure,plot(G);

upper_bounded = 0;
d=1/8;
force_symmetric = 0;

tic;[opt,x,x_edges] = strong_weak_links_cvx(Adj,upper_bounded,d,force_symmetric);toc

% The value of the objective:
sum(x)
% The trivial value of the objective by setting all strengths to medium:
length(is)/2
figure,plot(sort(x))
[x_edges x]


% Counterexample to symmetricity of optimum

Adj = zeros(16,16);
Adj(1:3,1:3)=1;
Adj(4,1:3)=1; Adj(1:3,4)=1;
Adj(5,1:3)=1; Adj(1:3,5)=1;
Adj(6,1:3)=1; Adj(1:3,6)=1;
Adj(7:16,6)=1; Adj(6,7:16)=1;
Adj(7:16,7:16)=1;
Adj = Adj-diag(diag(Adj));

[is,js,~] = find(triu(Adj));
G = graph(is,js,ones(length(is),1));
figure,plot(G);

upper_bounded = 0;
d=2;
force_symmetric = 0;

tic;[opt,x,x_edges] = strong_weak_links_cvx(Adj,upper_bounded,d,force_symmetric);toc

% The value of the objective:
sum(x)
% The trivial value of the objective by setting all strengths to medium:
length(is)/2
figure,plot(sort(x))
[x_edges x]

%% Karate dataset

%cd 'C:\Users\tidbie\Dropbox\My Research\2017_08_31 strong and weak links'
load('karate.mat')
Adj = Problem.A;
upper_bounded = 0;
d=1;
force_symmetric = 0;

tic;[opt,x,x_edges] = strong_weak_links_cvx(Adj,upper_bounded,d,force_symmetric);toc

G = graph(x_edges(:,1),x_edges(:,2),x);
LWidths = (0.5+G.Edges.Weight).^3/2;
figure,plot(G,...'EdgeLabel',round(G.Edges.Weight*4),
    'LineWidth',LWidths);

W = Adj;
for k=1:length(is)
    W(x_edges(k,1),x_edges(k,2)) = x(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x(k)+1; % +1, to distinguish weak edges from no edges
end
figure,imagesc(W)

% The value of the objective:
sum(x)
% The trivial value of the objective by setting all strengths to medium:
length(is)/2
figure,plot(sort(x))

% x = (x+1)/2; % This is needed because get_range_strong_weak_links_cvx still assumes that weak=0.5, medium=0.75, and strong=1.
% obj = sum(x);
% Ms = Adj;
% ms = Adj;
% for k=1:length(is)
%     [M,m] = get_range_strong_weak_links_cvx(Adj,obj,k);
%     Ms(x_edges(k,1),x_edges(k,2)) = M*4-2+1; % +1, to distinguish weak edges from no edges
%     Ms(x_edges(k,2),x_edges(k,1)) = M*4-2+1; % +1, to distinguish weak edges from no edges
%     ms(x_edges(k,1),x_edges(k,2)) = m*4-2+1; % +1, to distinguish weak edges from no edges
%     ms(x_edges(k,2),x_edges(k,1)) = m*4-2+1; % +1, to distinguish weak edges from no edges
% end
% 
% figure,subplot(1,3,1),imagesc(Ms)
% subplot(1,3,2),imagesc(ms)
% subplot(1,3,3),imagesc(Ms-ms)
% 
% x = x*4-2; % To map the strengths onto integers again.
% % If the following two numbers are equal to each other, then we can use the
% % MOSEK solution, and in particular which weights are non-integer, to
% % determine which weights can have more than 1 value within the optimal
% % face:
% length(find(abs(x-round(x))>1e-4)) % The number of weights that are not close to an integer value in the MOSEK solution in the relative interior (supposedly at least) of the optimal face.
% length(find(Ms-ms>1e-4))/2 % The number of weights for which the range of possible values in the optimal face is non-zero


%% Amazon politics books

cd 'C:\Users\tidbie\Dropbox\My Research\2017_08_31 strong and weak links'
edges = gml_to_matrix('./polbooks/polbooks.gml');
edges = edges(4:end,:) + 1;
Adj = sparse(edges(:,1),edges(:,2),1,105,105);
Adj = Adj+Adj';
clear edges
upper_bounded = 0;
d=1;
force_symmetric = 0;

tic;[opt,x,x_edges] = strong_weak_links_cvx(Adj,upper_bounded,d,force_symmetric);toc

G = graph(x_edges(:,1),x_edges(:,2),x);
LWidths = (0.5+G.Edges.Weight).^3/2;
figure,plot(G,...'EdgeLabel',round(G.Edges.Weight*4),
    'LineWidth',LWidths);

W = Adj;
for k=1:length(is)
    W(x_edges(k,1),x_edges(k,2)) = x(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x(k)+1; % +1, to distinguish weak edges from no edges
end
figure,imagesc(W)

% The value of the objective:
sum(x)
% The trivial value of the objective by setting all strengths to medium:
length(is)/2
figure,plot(sort(x))

% x = (x+1)/2; % This is needed because get_range_strong_weak_links_cvx still assumes that weak=0.5, medium=0.75, and strong=1.
% obj = sum(x);
% Ms = Adj;
% ms = Adj;
% for k=1:length(is)
%     [M,m] = get_range_strong_weak_links_cvx(Adj,obj,k);
%     Ms(x_edges(k,1),x_edges(k,2)) = M*4-2+1; % +1, to distinguish weak edges from no edges
%     Ms(x_edges(k,2),x_edges(k,1)) = M*4-2+1; % +1, to distinguish weak edges from no edges
%     ms(x_edges(k,1),x_edges(k,2)) = m*4-2+1; % +1, to distinguish weak edges from no edges
%     ms(x_edges(k,2),x_edges(k,1)) = m*4-2+1; % +1, to distinguish weak edges from no edges
% end
% 
% figure,subplot(1,3,1),imagesc(Ms)
% subplot(1,3,2),imagesc(ms)
% subplot(1,3,3),imagesc(Ms-ms)
% 
% x = x*4-2; % To map the strengths onto integers again
% % If the following two numbers are equal to each other, then we can use the
% % MOSEK solution, and in particular which weights are non-integer, to
% % determine which weights can have more than 1 value within the optimal
% % face:
% length(find(abs(x-round(x))>1e-4)) % The number of weights that are not close to an integer value in the MOSEK solution in the relative interior (supposedly at least) of the optimal face.
% length(find(Ms-ms>0.5))/2 % The number of weights for which the range of possible values in the optimal face is non-zero


%% Les miserables

cd 'C:\Users\tidbie\Dropbox\My Research\2017_08_31 strong and weak links'
edges = gml_to_matrix('./datasets/lesmis/lesmis.gml');
edges = edges(5:end-1,:) + 1;
Adj = sparse(edges(:,1),edges(:,2),1,77,77);
Adj = Adj+Adj';
clear edges
upper_bounded = 0;
d=1;
force_symmetric = 1;

tic;[opt,x,x_edges] = strong_weak_links_cvx_2var(Adj,upper_bounded,d,force_symmetric);toc

G = graph(x_edges(:,1),x_edges(:,2),x);
LWidths = (0.5+G.Edges.Weight).^3/2;
figure,plot(G,...'EdgeLabel',round(G.Edges.Weight*4),
    'LineWidth',LWidths);

W = Adj;
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x(k)+1; % +1, to distinguish weak edges from no edges
end
figure,imagesc(W)


% The value of the objective:
sum(x)
% The trivial value of the objective by setting all strengths to medium:
length(is)/2
figure,plot(sort(x))

% x = (x+1)/2; % This is needed because get_range_strong_weak_links_cvx still assumes that weak=0.5, medium=0.75, and strong=1.
% obj = sum(x);
% Ms = Adj;
% ms = Adj;
% for k=1:length(is)
%     [M,m] = get_range_strong_weak_links_cvx(Adj,obj,k);
%     Ms(x_edges(k,1),x_edges(k,2)) = M*4-2+1; % +1, to distinguish weak edges from no edges
%     Ms(x_edges(k,2),x_edges(k,1)) = M*4-2+1; % +1, to distinguish weak edges from no edges
%     ms(x_edges(k,1),x_edges(k,2)) = m*4-2+1; % +1, to distinguish weak edges from no edges
%     ms(x_edges(k,2),x_edges(k,1)) = m*4-2+1; % +1, to distinguish weak edges from no edges
% end
% 
% figure,subplot(1,3,1),imagesc(Ms)
% subplot(1,3,2),imagesc(ms)
% subplot(1,3,3),imagesc(Ms-ms)
% 
% x = x*4-2; % To map the strengths onto integers again
% % If the following two numbers are equal to each other, then we can use the
% % MOSEK solution, and in particular which weights are non-integer, to
% % determine which weights can have more than 1 value within the optimal
% % face:
% length(find(abs(x-round(x))>1e-4)) % The number of weights that are not close to an integer value in the MOSEK solution in the relative interior (supposedly at least) of the optimal face.
% length(find(Ms-ms>0.5))/2 % The number of weights for which the range of possible values in the optimal face is non-zero


%% Facebook

edges = dlmread('facebook_combined.txt')+1;
Adj = sparse(edges(:,1),edges(:,2),1,max(max(edges)),max(max(edges)));
Adj = Adj+Adj';
clear edges
upper_bounded = 0;
d=1;
force_symmetric = 0;

tic;[opt,x,x_edges] = strong_weak_links_cvx(Adj,upper_bounded,d,force_symmetric);toc

[is,js,~] = find(triu(Adj));
% G = graph(is,js,x);
% LWidths = (0.5+G.Edges.Weight).^3/2;
% figure,plot(G,...'EdgeLabel',round(G.Edges.Weight*4),
%     'LineWidth',LWidths);

W = Adj;
for k=1:length(is)
    W(x_edges(k,1),x_edges(k,2)) = x(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x(k)+1; % +1, to distinguish weak edges from no edges
end
figure,imagesc(W)

% The value of the objective:
sum(x)
% The trivial value of the objective by setting all strengths to medium:
length(is)/2
figure,plot(sort(x))

% x = (x+1)/2; % This is needed because get_range_strong_weak_links_cvx still assumes that weak=0.5, medium=0.75, and strong=1.
% obj = sum(x);
% Ms = Adj;
% ms = Adj;
% % N
% for k=1:length(is)
%     if abs((x(k)*4-2)-round(x(k)*4-2))<1e-4 % the weight found by MOSEK in the relative interior solution is integer and thus it is assumed to be the only possible value on the optimal face
%         M = x(k);
%         m = x(k);
%     else
%         [M,m] = get_range_strong_weak_links_cvx(Adj,obj,k);
%     end
%     Ms(x_edges(k,1),x_edges(k,2)) = M*4-2+1; % +1, to distinguish weak edges from no edges
%     Ms(x_edges(k,2),x_edges(k,1)) = M*4-2+1; % +1, to distinguish weak edges from no edges
%     ms(x_edges(k,1),x_edges(k,2)) = m*4-2+1; % +1, to distinguish weak edges from no edges
%     ms(x_edges(k,2),x_edges(k,1)) = m*4-2+1; % +1, to distinguish weak edges from no edges
% end
% 
% figure,subplot(1,3,1),imagesc(Ms)
% subplot(1,3,2),imagesc(ms)
% subplot(1,3,3),imagesc(Ms-ms)


%%

%cd 'C:\Users\tidbie\Dropbox\My Research\2017_08_31 strong and weak links'
%M = dlmread('./datasets/students.csv', ';');
M = dlmread('./datasets/enron.csv', ';');
%M = dlmread('./datasets/facebookBig.csv', ';');
%M = dlmread('./datasets/twitterBig.csv', ';');
%M = dlmread('./datasets/tumblr.csv', ';');
%M = dlmread('enron.csv', ';');
edges = M(:,[1 2]);
Adj = sparse(edges(:,1),edges(:,2),1,max(max(edges)),max(max(edges)));
Adj = Adj+Adj';
clear edges
upper_bounded = 0;
d=2;
force_symmetric = 0;

tic;[opt,x,x_edges] = strong_weak_links_cvx(Adj,upper_bounded,d,force_symmetric);toc

[is,js,~] = find(triu(Adj));
G = graph(is,js,x);
LWidths = (0.5+G.Edges.Weight).^3/2;
figure,plot(G,...'EdgeLabel',round(G.Edges.Weight*4),
    'LineWidth',LWidths);

W = Adj;
for k=1:length(is)
    W(x_edges(k,1),x_edges(k,2)) = x(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x(k)+1; % +1, to distinguish weak edges from no edges
end
figure,imagesc(W)

% The value of the objective:
sum(x)
% The trivial value of the objective by setting all strengths to medium:
length(is)/2
figure,plot(sort(x))
