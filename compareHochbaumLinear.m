%% Read toy data
%edges = dlmread('./datasets/toy.csv', ';');
%edges = dlmread('./datasets/toy2.csv', ';');


n = 10;
A = triu(randi([0 1], n, n));
A(1:1+size(A,1):end) = 0;
[i, j] = find(A);
edges = [i,j];
A = computeAdjMatrix(edges);

%% Compute linear solution
%gcp('nocreate');
%addpath('/Users/jlijffijt/matlab/cvx/',...
%    '/Users/jlijffijt/matlab/cvx/functions/', ...
%    '/Users/jlijffijt/matlab/cvx/lib/');
%%
upper_bounded = 0;
d=2;


tic;
[opt,x,x_edges] = ...
    strong_weak_links_cvx_2var(A, upper_bounded, d, 1);
toc

var2Solution = sparse(x_edges(:,1),x_edges(:,2),x,size(A,1),size(A,2));
var2Solution = var2Solution + var2Solution';
var2Solution = triu(var2Solution);


%% Compute Hochbaum solution
edges = edges(:, [1 2]);
Hsolution = STCHochbaum(edges, d);

Hsolution = triu(Hsolution);

diff = sum(abs(Hsolution(:) - var2Solution(:)));
diff

%% Get edge values
%[i,j,v] = find(W+0.5);
%h_edges = [i j];
%pos = v >= 0 & (h_edges(:,2) > h_edges(:,1));
%h_edges = h_edges(pos,:);
%v = v(pos)-0.5;

%clear i j pos;

%% Print
fprintf('%s\n',['Linear opt: ' num2str(opt)]);
%fprintf('%s\n',['Hochbaum opt: ' num2str(sum(v))]);
fprintf('%s\n',['Hochbaum opt: ' num2str(sum(Hsolution(:)))]);