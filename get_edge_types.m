function [wedges, triangleVs] = get_edge_types(Adj_counter_red, representatives)
%%
    Adj = double(logical(Adj_counter_red + Adj_counter_red'));

    Adj_counter_red = Adj_counter_red + Adj_counter_red';
    %%

    % Enumerate all pairs of endpoints of wedges and triangleVs, and count the
    % number of wedges and triangleVs

    Adj2 = Adj*Adj;
    Adj2 = Adj2-diag(diag(Adj2));
    [wedgeends1,wedgeends2,~] = find(triu(Adj)==0 & triu(Adj2)>0);
    nwedges = sum(sum(triu(Adj2).*(1-Adj)));

    % For each pair of endpoints, enumerate all wedges and triangleVs with these two endpoints

    wedges = zeros(nwedges,2); % The wedge edges
    wedgesv = zeros(nwedges,3); % The wedge vertices (middle one is the point of the wedge)

    startind = 1;
    for i=1:length(wedgeends1)
        endind = startind+Adj2(wedgeends1(i),wedgeends2(i))-1;
        midpoints = find(Adj(:,wedgeends1(i))>0 & Adj(:,wedgeends2(i))>0);
        wedgesv(startind:endind,1) = wedgeends1(i);
        wedgesv(startind:endind,2) = midpoints;
        wedgesv(startind:endind,3) = wedgeends2(i);
        for j=startind:endind
            wedges(j,1) = Adj_counter_red(wedgesv(j,1),wedgesv(j,2));
            wedges(j,2) = Adj_counter_red(wedgesv(j,2),wedgesv(j,3));
        end
        startind = endind+1;
    end
    n_triangle_cliques = length(representatives);
    triangleVs = zeros(sum(sum(Adj(1:n_triangle_cliques,:))),2); % The first column will contain the index of the triangle_clique, the second column will contain the index of the wedge-edge
    ct = 1;
    for i=1:n_triangle_cliques
        for j=find(Adj(i,:))
            triangleVs(ct,:) = [i Adj_counter_red(i,j)];
            ct = ct+1;
        end
    end
    ntriangleVs = size(triangleVs,1);

    %all_edges = unique(Adj_counter(:));
    %all_edges(all_edges==0)=[];
    %wedge_edges = unique(wedges(:))';
    %tri_only_edges = full(setdiff(all_edges, wedge_edges)');

    %tr_indicators = ismember(triangleVs, tri_only_edges);
    %clique_edges = triangleVs(find(sum(tr_indicators, 2) == 3),:);
    %clique_edges = unique(clique_edges(:))';
    %tri_only_edges = setdiff(tri_only_edges, clique_edges);
end

