function h = plotRangeComparison(n, x_edges, x_min, x_max)
figure('Position',[100 300 1200 400]);

lm = 0.03;
rm = 0.065;
hm = 0.01;
bm = 0.08;
tm = 0.065;
vm = 0;
fs = 20;

plotSubFigure(lm, rm, hm, bm, tm, vm, 1, 3, fs, 2, true);
Z = computeAdjMatrix(x_edges, x_max+1);
imagesc(Z-1,[-1 round(max(x_max))]);
title('Maximum values');
xlim([0.5 n+0.5]);
ylim([0.5 n+0.5]);
set(gca,'YTick',[]);
set(gca,'YDir','reverse');

plotSubFigure(lm, rm, hm, bm, tm, vm, 1, 3, fs, 1, true);
Y = computeAdjMatrix(x_edges, x_min+1);
imagesc(Y-1,[-1 round(max(x_max))]);
title('Minimum values');
xlim([0.5 n+0.5]);
ylim([0.5 n+0.5]);
set(gca,'YDir','reverse');

plotSubFigure(lm, rm, hm, bm, tm, vm, 1, 3, fs, 3, true);
X = computeAdjMatrix(x_edges, (x_max-x_min)+1);
imagesc(X-1,[-1 round(max(x_max))]);
title('Difference');
xlim([0.5 n+0.5]);
ylim([0.5 n+0.5]);
set(gca,'YTick',[]);
set(gca,'YDir','reverse');

colorbar('Location','manual','Position',[0.945 bm 0.02 1-bm-tm],'Limits',[-1 round(max(x_max))]);

end