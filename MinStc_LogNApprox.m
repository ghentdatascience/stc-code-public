function [weakEdges, margin] = MinStc_LogNApprox(A)
% Assuming A is the wedge graph, this will return a list of nodes (weak edges)
% as found by the log n approximation. Based on greedily finding the edge
% that is part of the most wedge constraints (highest degree), and then
% removing incident edges and repeat.

% Init
A = (A | A');
margin = sum(A);
weakEdges = zeros(1,size(A,1));
c = 0;

% Run iterations
while(sum(margin) > 0)
    c = c + 1;
    % Take largest node
    [~,max_m] = max(margin);
    % Find connected nodes
    q = find(A(:,max_m));
    % Update
    weakEdges(c) = max_m;
    margin(q) = margin(q) - sign(margin(q));
    margin(max_m) = 0;
end

% Take only part that was used
weakEdges = weakEdges(1:c);

end