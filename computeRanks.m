function ranks = computeRanks(R)

[~,ix] = sort(R,'descend');
[~,ranks] = sort(ix,'ascend');

for i = 1:length(ranks)-1
    if(R(ix(i)) == R(ix(i+1)))
        ranks(ix(i+1)) = ranks(ix(i));
    end
end

end