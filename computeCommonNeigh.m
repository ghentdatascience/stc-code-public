function common = computeCommonNeigh(A, edgeList)
% Compute the common neighbors for the edges in edgeList.
A=A|A';
l = length(edgeList);
common = zeros(l,1);
for i=1:l
    n_1 = find(A(edgeList(i,1),:));
    n_2 = find(A(edgeList(i,2),:));
    common(i) = length(intersect(n_1,n_2));
end
end