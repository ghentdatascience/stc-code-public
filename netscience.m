%% Read data
A = readNetscienceData();
edges = getEdges(A);

%% Try to compute ranges
gcp('nocreate');
addpath('/Users/jlijffijt/matlab/cvx/',...
    '/Users/jlijffijt/matlab/cvx/functions/', ...
    '/Users/jlijffijt/matlab/cvx/lib/');

upper_bounded = 0;
d=2;

tic;
[opt,x_min,x_max,x_edges] = ...
    strong_weak_links_cvx_ranges(A,upper_bounded,d);
toc

%% Store results
clear ans;

save netscience_results.mat;

%% Load results
load netscience_results.mat;

%% Plot both x_min and x_max as graph
plotRangeComparison(size(A,1), x_edges, x_min, x_max)

%% Test equivalence of Hochbaum solution
% Compute Hochbaum solution
W = STCHochbaum(edges, d);

%% Get edge values
[i,j,v] = find(W+0.5);
h_edges = [i j];
pos = v >= 0 & (h_edges(:,2) > h_edges(:,1));
h_edges = h_edges(pos,:);
v = v(pos)-0.5;

clear i j pos;

%% Compare edge sets
