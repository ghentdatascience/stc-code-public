function h = runToyGraph()
% A toygraph with two communities and a bridge.
% A plot of C vs the number of wedge violations.
A = [0 0 1 1 0 0 0 0; 0 0 1 1 0 0 0 0; 1 1 0 1 0 0 0 0; 1 1 1 0 1 0 0 0; 0 0 0 1 0 1 1 1; 0 0 0 0 1 0 1 1; 0 0 0 0 1 1 0 1; 0 0 0 0 1 1 1 0];
% Critical C-value for boundedness is 1/6;
C1 = 2:0.1:4;
violations1 = zeros(length(C1),1);
for i=1:length(C1)
    [~,~,epsilon,~,~] = strong_weak_links_cvx_slacks(A,0,1,C1(i),0);
    violations1(i) = length(find(epsilon>-0.999));
end

plot(C1,violations1)
end