function weakEdges = MinStc_TwoApprox(A)
% Assuming A is the wedge graph, this will return a list of nodes (weak edges)
% as found by the 2 approximation. Based on greedily finding a maximal matching and
% selecting all nodes in that matching (edges).

% Init
d = full(sum(A));
c = 0;
weakEdges = zeros(1,length(d));

% Iterate
while(sum(d) > 0)
    % Find max degree node i
    [~,max_d] = max(d);
    % Find connected max degree node j
    connected = find(A(:,max_d));
    [~,max_d2_ix] = max(d(connected));
    max_d2 = connected(max_d2_ix);
    % Find nodes connected to node j
    connected2 = find(A(:,max_d2));
    % Update
    c = c + 2;
    weakEdges(c-1) = max_d;
    weakEdges(c) = max_d2;
    d(connected) = d(connected) - sign(d(connected));
    d(connected2) = d(connected2) - sign(d(connected2));
    d(max_d) = 0;
    d(max_d2) = 0;
end

weakEdges = weakEdges(1:c);

end