function [AGiant, gcc] = computeGiantComponent(A)
% Computing the adj. matrix of the largest CC.
% gcc is a list of nodes that are in the largest CC.

A = A - diag(diag(A));
A = (A | A')*1.0;
A_orig = A;
% Take stupid approach to compute giant CC
% This will relabel the vertices!
iterate = true;
count = sum(sum(A));
while(iterate)
    A = (A|((A_orig*A)>0))*1.0;
    new_count = sum(sum(A));
    if(new_count > count)
        count = new_count;
    else
        iterate = false;
    end
end
% Take subset
[~,i] = max(sum(A));
gcc = find(A(:,i));
% Expand graph to full adjecency matrix
AGiant = A_orig(gcc,gcc);
end