function edges = getEdges(Adj)
% Compute edge set from adjacency matrix
[i,j] = find(Adj);
edges = [i j];
end