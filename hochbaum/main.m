%% Init
M = dlmread('./../datasets/toy.csv', ';');
% M = dlmread('./../datasets/students.csv', ';');
%M = dlmread('./datasets/enron.csv', ';');
%M = dlmread('./datasets/facebookBig.csv', ';');
%M = dlmread('./datasets/twitterBig.csv', ';');
%M = dlmread('./datasets/tumblr.csv', ';');
% M = dlmread('./../datasets/enron.csv', ';');
edges = M(:, [1 2]);
clear M

d = 2;

%% Compute solution
A = STCHochbaum(edges, d);

%% Visualize
S = graph(A);
figure(3);
imagesc(A)
colorbar
%plot(S, 'EdgeLabel', S.Edges.Weight);
fprintf('%s\n',['Total cost: ' num2str(full(sum(A(:)))/2)]);