function [v, n_match] = somersD(R, G)
% Compute somers' D: assymetric Kendall-tau statistic where a correction
% for ties in the ground truth ordinal statistics G is made but not for
% ties in the reproduced ordinal statistics R

n = length(R);
if(length(G) ~= n)
    error('R and G need to have same length.');
end

rankR = computeRanks(R);
rankG = computeRanks(G);

n_poss = sum(rankG-1);
if(n_poss == 0)
    error('Ground truth should contain at least two unique values.')
end

n_match = 0;

for i = 1:n-1
    for j = i+1:n
        switch sign(rankG(i) - rankG(j))
            case 1
                % If agree +1, if tied or reverse -1
                n_match = n_match + sign(rankR(i) - rankR(j));
            case -1
                % If agree +1, if tied or reverse -1
                n_match = n_match - sign(rankR(i) - rankR(j));
            case 0
                % Nothing
        end
    end
end

v = n_match / n_poss;

end