function h = testAmazon()
M = dlmread('./datasets/DataSets/AmazonBooks/BooksEdges.txt','\t');
edges = [M(:,1)+1 M(:,2)+1];

A = full(sparse(edges(:, 1), edges(:, 2), 1, 105, 105));
A=A|A';

% The communities
fid   = fopen('./datasets/DataSets/AmazonBooks/BooksType.txt');
line = textscan(fid, '%f%s %*[^\n]');
fclose(fid);

idx_n = find(ismember(line{2}, 'n'));
idx_c = find(ismember(line{2}, 'c'));
idx_l = find(ismember(line{2}, 'l'));

% The model
[~,x,x_edges] = strong_weak_links_cvx_2var(A,0,1,1);
%[~,x,epsilon,x_edges,epsilon_edges] = strong_weak_links_cvx_slacks(A,0,1,1,0);

interE = [];
intraE = [];
for i=1:nnz(A)/2 
    if (ismember(x_edges(i,1),idx_n) && ismember(x_edges(i,2),idx_c)) || (ismember(x_edges(i,1),idx_c) && ismember(x_edges(i,2),idx_n))
        interE = [interE; [x_edges(i,1) x_edges(i,2)]];
    elseif (ismember(x_edges(i,1),idx_c) && ismember(x_edges(i,2),idx_l)) || (ismember(x_edges(i,1),idx_l) && ismember(x_edges(i,2),idx_c))
        interE = [interE; [x_edges(i,1) x_edges(i,2)]];
    elseif (ismember(x_edges(i,1),idx_l) && ismember(x_edges(i,2),idx_n)) || (ismember(x_edges(i,1),idx_n) && ismember(x_edges(i,2),idx_l))
        interE = [interE; [x_edges(i,1) x_edges(i,2)]];
    else
        intraE = [intraE; [x_edges(i,1) x_edges(i,2)]];
    end
end

[~,ind_inter]=ismember(interE,x_edges,'rows');
[~,ind_intra]=ismember(intraE,x_edges,'rows');

% average weights over inter edges:
sum(x(ind_inter))/70
sum(x(ind_intra))/371
end