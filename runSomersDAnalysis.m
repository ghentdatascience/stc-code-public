function h = runSomersDAnalysis(A, edges, v)

% cvx_solver mosek

% Init
d = 1:1;
C = 1:1;

% LP2-STC and LP4-STC
kt = computeSomersDSeries(A,edges,v,d);
kts = computeSomersDSeriesSlack(A,edges,v,d,C);

% Sintos 2-approximation
[A_wedge, wedgeToEdgeList] = get_wedge_Adj(A);

weak_ind = MinStc_TwoApprox(A_wedge);
weakEdges = wedgeToEdgeList(weak_ind,:); % the weak edges
strong = ones(length(v),1);
weakEdges = [weakEdges; [weakEdges(:,2) weakEdges(:,1)]]; % accounting for undirected edges.
strong(ismember(edges,weakEdges,'rows'))=0;
coeff_2app = somersD(strong,v);

% Sintos logn-approximation
weak_ind = MinStc_LogNApprox(A_wedge);
weakEdges = wedgeToEdgeList(weak_ind,:); % the weak edges
strong = ones(length(v),1);
weakEdges = [weakEdges; [weakEdges(:,2) weakEdges(:,1)]]; % accounting for undirected edges.
strong(ismember(edges,weakEdges,'rows'))=0;
coeff_lognapp = somersD(strong,v);

% Common Neighbors.
common = computeCommonNeigh(A, edges);
coeff_common = somersD(common,v);

% Visualize
figure;
% No slack
h = plot(d,kt,'o-');
legendlabels = cell(1,length(C));
legendlabels{1} = 'No slack';
hold on;

% Slack
for i = 1:length(C)
    plot(d,kts(i,:),'o-');
    legendlabels{i+1} = ['C = ' num2str(C(i))];
end

% Sintos 2-approx. plot.
plot(d, coeff_2app*ones(1,length(d)), 'o-');
legendlabels{length(C)+2} = 'Sintos 2-app';

% Sintos log(n)-approx. plot.
plot(d, coeff_lognapp*ones(1,length(d)), 'o-');
legendlabels{length(C)+3} = 'Sintos log(n)-app';

% Common neighbors. plot.
plot(d, coeff_common*ones(1,length(d)), 'o-');
legendlabels{length(C)+4} = 'CommonNeighbors';

ylabel('Kendall tau');
xlabel('d');
legend(legendlabels);

end