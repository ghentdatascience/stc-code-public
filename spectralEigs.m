function [V,E] = spectralEigs(A)

n = size(A,1);

% Compute Laplacian
D = zeros(size(A));
t = full(sum(A));
for i = 1:n
    D(i,i) = t(i);
end
L = D - A;

[V,E] = eig(L);

end