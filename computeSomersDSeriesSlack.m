function sd = computeSomersDSeriesSlack(A, edges, v, d, C)

% CVXPATH = '\\Client\C$\Users\fawadria\matlab\cvx\';
CVXPATH = '/Users/jlijffijt/matlab/cvx/';
gcp('nocreate');
% addpath(CVXPATH, [CVXPATH 'functions\'], [CVXPATH 'lib\']);
addpath(CVXPATH, [CVXPATH 'functions/'], [CVXPATH 'lib/']);
% cvx_solver mosek

% Init
n_d = length(d);
n_C = length(C);
sd = zeros(n_C,n_d);

for i = 1:n_C
    for j = 1:n_d
        % Compute symmetric optimal solution
        [~,x,~,x_edges,~] = strong_weak_links_cvx_slacks(A,0,d(j),C(i),0);
        x = round(x*2)/2;

        % Give index of edges to map to x_edges
        ix = alignEdges(edges,x_edges);

        % Compute Somers' D rank correlation
        sd(i,j)  = somersD(x(ix),v);
    end
end

end