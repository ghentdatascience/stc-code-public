function z = minimize_edge(edge, K, opt, Awedges, bwedges, AtriangleVs, btriangleVs, upper_bounded)

cvx_begin
    variable x(K) nonnegative %integer
    minimize( x(edge) ) % With MOSEK also this can be used, as apparently MOSEK prefers 'integral' solutions (although not necessarily the ones with smallest norm, which is in fact a good thing if we want to maximize the number of strong links)
    subject to
        sum(x) == opt %#ok<NOPRT,EQEFF>
        Awedges*x <= bwedges  %#ok<NOPRT>
        AtriangleVs*x <= btriangleVs  %#ok<NOPRT>
        if upper_bounded
            x <= 1 %#ok<NOPRT> % % This constraint can be removed if more than weak/medium/strong levels are requested (i.e. stronger levels than strong)
        end
cvx_end

z = cvx_optval;

end