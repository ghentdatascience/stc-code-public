function [M,m] = get_range_strong_weak_links_cvx(Adj,obj,branch)

% function [M,m] = get_range_strong_weak_links_cvx(Adj,obj,branch)
%
% This function finds the upper (M) and lower (m) bound on the strength of
% the inputted branch, given a known optimal value of the objective (in the
% non-integer formulation, i.e. with weights being 0.5, 0.75, or 1), and
% this given the (sparse) adjacency matrix Adj.


[is,js,~]=find(triu(Adj));
K = length(is);
N = size(Adj,1);

Adj_counter = triu(Adj);
Adj_counter(Adj_counter>0) = 1:sum(sum(Adj_counter));
Adj_counter = Adj_counter + Adj_counter';

increment = 10^(max(0,round(log10(K))-2));

% Count how many constraints there are, so we can initialize A and b

ncons=0;
for k12=1:K
    if floor(k12/increment)==k12/increment
        disp([num2str(k12) ' of ' num2str(K)])
    end
    e1 = is(k12);
    e2 = js(k12);
    num_adj1 = sum(Adj(e1,1:e2-1));
    num_adj2 = sum(Adj(e2,e1+1:end));
    ncons = ncons+num_adj1+num_adj2;
end
Ar = zeros(3*ncons,1);
Ac = zeros(3*ncons,1);
Av = zeros(3*ncons,1);

% Fill in values in A and b

ct = 1;
row = 1;
for k12=1:K
    if floor(k12/increment)==k12/increment
        disp([num2str(k12) ' of ' num2str(K)])
    end
    e1 = is(k12);
    e2 = js(k12);
    adj1 = find(Adj(e1,1:e2-1));
    adj2 = e1+find(Adj(e2,e1+1:end));
    for j=1:length(adj1)
        e0 = adj1(j);
        k01 = Adj_counter(e0,e1);
        Ac(ct) = k01;
        Ar(ct) = row;
        Av(ct) = 1;
        ct = ct+1;
        Ac(ct) = k12;
        Ar(ct) = row;
        Av(ct) = 1;
        ct = ct+1;
        if Adj(e0,e2)
            k02 = Adj_counter(e0,e2);
            Ac(ct) = k02;
            Ar(ct) = row;
            Av(ct) = -1;
            ct = ct+1;
        end
        row = row+1;
    end
    for j=1:length(adj2)
        e3 = adj2(j);
        k23 = Adj_counter(e2,e3);
        Ac(ct) = k12;
        Ar(ct) = row;
        Av(ct) = 1;
        ct = ct+1;
        Ac(ct) = k23;
        Ar(ct) = row;
        Av(ct) = 1;
        ct = ct+1;
        if Adj(e1,e3)
            k13 = Adj_counter(e1,e3);
            Ac(ct) = k12;
            Ar(ct) = row;
            Av(ct) = -1;
            ct = ct+1;
        end
        row = row+1;
    end
end
Arcv = unique([Ar(1:ct-1) Ac(1:ct-1) Av(1:ct-1)],'rows');
A = sparse(Arcv(:,1),Arcv(:,2),Arcv(:,3),max(Arcv(:,1)),K);
b = 1.5*ones(size(A,1),1);

% Specify problem and run solver

cvx_solver mosek
cvx_begin
    variable x(K) nonnegative
    maximize( x(branch) )
    subject to
        sum(x) == obj
        A*x <= b
        x <= 1
cvx_end
M = x(branch);
cvx_begin
    variable x(K) nonnegative
    minimize( x(branch) )
    subject to
        sum(x) == obj
        A*x <= b
        x <= 1
cvx_end
m = x(branch);
