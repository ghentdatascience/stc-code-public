function weakEdges = MinStc_TwoApproxWithTiebreaking(A, nclosed)
% Assuming A is the wedge graph, this will return a list of nodes (weak edges)
% as found by the 2 approximation. Based on greedily finding a maximal matching and
% selecting all nodes in that matching (edges).

% Init
d = full(sum(A));
c = 0;
weakEdges = zeros(1,length(d));

% Iterate
while(sum(d) > 0)
    % Find max degree node i
    v = max(d);
    cand = find(d == v);
    [~,cand_min_closed] = min(nclosed(cand));
    max_d = cand(cand_min_closed);
    % Find connected max degree node j
    connected = find(A(:,max_d));
    v = max(d(connected));
    cand_ix = find(d(connected) == v);
    [~,cand_min_closed] = min(nclosed(connected(cand_ix)));
    max_d2 = connected(cand_ix(cand_min_closed));
    % Find nodes connected to node j
    connected2 = find(A(:,max_d2));
    % Update
    c = c + 2;
    weakEdges(c-1) = max_d;
    weakEdges(c) = max_d2;
    d(connected) = d(connected) - sign(d(connected));
    d(connected2) = d(connected2) - sign(d(connected2));
    d(max_d) = 0;
    d(max_d2) = 0;
end

weakEdges = weakEdges(1:c);

end