function h = createFiguresLM()
[A,edges,v] = readLMData();
limits = [-1 2];


figure('Position',[100 100 800 900]);

lm = 0.06;
rm = 0.01;
hm = 0.015;
bm = 0.15;
tm = 0.04;
vm = 0.05;
fs = 20;

% Sintos Greedy heatmap.
tic;
[A_wedge, wedgeToEdgeList] = get_wedge_Adj(A);
toc;
tic;
weak_ind = MinStc_LogNApprox(A_wedge);
toc;
weakEdges = wedgeToEdgeList(weak_ind,:); % the weak edges
strong = ones(length(v),1);
weakEdges = [weakEdges; [weakEdges(:,2) weakEdges(:,1)]]; % accounting for undirected edges.
strong(ismember(edges,weakEdges,'rows'))=0;

W = A;
W(~W)=-1; % absent edge = -1 in the heatmap.
for k=1:size(strong,1)
    W(edges(k,1),edges(k,2)) = strong(k); 
    W(edges(k,2),edges(k,1)) = strong(k); 
end
plotSubFigure(lm, rm, hm, bm, tm, vm, 2, 2, fs, 1, true)
imagesc(W)
set(gca,'XTickLabel',{});
%colorbar
caxis(limits)
xlim([0.5 77.5]);
ylim([0.5 77.5]);
title('STCBinary');
% LP1-STC
upper_bounded = 1;
d=1;
force_symmetric = 1;
[~,x,x_edges] = strong_weak_links_cvx_2var(A,upper_bounded,d,force_symmetric);

W = A;
W(~W)=-1; % absent edge = -1 in the heatmap.
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x(k);
    W(x_edges(k,2),x_edges(k,1)) = x(k); 
end
plotSubFigure(lm, rm, hm, bm, tm, vm, 2, 2, fs, 2, true)
imagesc(W)
set(gca,'XTickLabel',{});
set(gca,'YTickLabel',{});
%colorbar
caxis(limits)
xlim([0.5 77.5]);
ylim([0.5 77.5]);
title('LP1');

% LP2-STC d=1.
upper_bounded = 0;
d=1;
force_symmetric = 1;
[~,x,x_edges] = strong_weak_links_cvx_2var(A,upper_bounded,d,force_symmetric);

W = A;
W(~W)=-1; % absent edge = -1 in the heatmap.
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x(k);
    W(x_edges(k,2),x_edges(k,1)) = x(k);
end
plotSubFigure(lm, rm, hm, bm, tm, vm, 2, 2, fs, 3, true)
imagesc(W)
%colorbar
caxis(limits)
xlim([0.5 77.5]);
ylim([0.5 77.5]);
title('LP2 (d=1)');

% LP4-STC d=1, C=1.
upper_bounded = 0;
d=1;
C=1;
force_symmetric = 1;
[~,x,epsilon,x_edges,epsilon_edges] = strong_weak_links_cvx_slacks(A,upper_bounded,d,C,force_symmetric);

W = A;
W(~W)=-1; % absent edge = -1 in the heatmap.

for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x(k);
    W(x_edges(k,2),x_edges(k,1)) = x(k);
end

for k=1:size(epsilon_edges,1)
    W(epsilon_edges(k,1),epsilon_edges(k,2)) = epsilon(k);
    W(epsilon_edges(k,2),epsilon_edges(k,1)) = epsilon(k);
end

plotSubFigure(lm, rm, hm, bm, tm, vm, 2, 2, fs, 4, true)
imagesc(W)
set(gca,'YTickLabel',{});
h = colorbar('southoutside');
set(h,'Location','manual');
set(h,'Position',[0.20 0.042 0.60 0.05]);
set(h,'Ticks',-1:2);
caxis(limits)
xlim([0.5 77.5]);
ylim([0.5 77.5]);
title('LP4 (d=1,C=1)');

end