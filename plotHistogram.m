function h = plotHistogram(values)
% Plot nice histogram of weights over all edges

r = round(values,2);

a = min(r);
b = max(r);

intv = (floor(a*2)/2-0.05):0.1:(ceil(b*2)/2)+0.05;

h = histogram(r,intv);
xlim([min(intv) max(intv)]);
set(gca,'XTick',floor(a*2)/2:0.5:(ceil(b*2)/2));
xlabel('Value');

scale = 10^floor(log10(max(h.Values)));
ylim([0 ceil(max(h.Values)/scale)*scale]);
set(gca,'YTick',0:scale:ceil(max(h.Values)/scale)*scale);
ylabel('Number of occurrences');

set(gca,'TickDir','out');
box off;

end