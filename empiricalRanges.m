function h = empiricalRanges(A, edges, v)
% Run 3 different solvers to g

cvx_solver mosek
cvx_solver_settings('MSK_IPAR_INTPNT_BASIS','MSK_BI_NEVER')
[~,x1,x1_edges] = strong_weak_links_cvx(A,0,1,0); % force symmetric has to be 0 to find interior points.
x1 = round(1000*x1)/1000; % Rounding the numerical errors to 0, 1/2, 1, 2, ... while leaving the inbetween values unchanged.
ix1 = alignEdges(edges,x1_edges);
x1 = x1(ix1); % aligning the x-values.

cvx_solver SDPT3
[~,x2,x2_edges] = strong_weak_links_cvx(A,0,1,0);
x2 = round(1000*x2)/1000;
ix2 = alignEdges(edges,x2_edges);
x2 = x2(ix2); % aligning the x-values.

cvx_solver SeDuMi
[~,x3,x3_edges] = strong_weak_links_cvx(A,0,1,0);
x3 = round(1000*x3)/1000;
ix3 = alignEdges(edges,x3_edges);
x3 = x3(ix3); % aligning the x-values.

figure
plot(sort(x1))
hold on
plot(sort(x2))
hold on
plot(sort(x3))

I = zeros(7, length(v));
% [0,0] interval.
I(1,:) = x1==0 & x2==0 & x3==0;

% [0,1/2] interval.
I(2,:) = x1<=1/2 & x2<=1/2 & x3<=1/2 & ~(x1==0 & x2==0 & x3==0) & ~(x1==1/2 & x2==1/2 & x3==1/2);

% [0,1] interval.
I(3,:) = (x1<1/2 | x2<1/2 | x3<1/2) & (x1>1/2 | x2>1/2 | x3>1/2);

% [1/2,1/2] interval.
I(4,:) = x1==1/2 & x2==1/2 & x3==1/2;

% [1/2,1] interval.
I(5,:) = (x1>=1/2 & x2>=1/2 & x3>=1/2 & x1<=1 & x2<=1 & x3<=1) & ~(x1==1 & x2==1 & x3==1) & ~(x1==1/2 & x2==1/2 & x3==1/2);

% [1,1] interval.
I(6,:) = x1==1 & x2==1 & x3==1;

% [2,2] interval.
I(7,:) = x1==2 & x2==2 & x3==2;

numberofEdges = sum(I,2); 
meanWeight = zeros(7,1);
for i=1:7
    meanWeight(i) = sum(v(I(i,:)==1));
end
meanWeight = meanWeight./numberofEdges;

rankByIntervals = zeros(1,length(v));
for i=1:3
    rankByIntervals(I(i,:)==1)=i;
end
rankByIntervals(I(i,4)==1)=3; % The intervals [0 1] and [1/2 1/2] are given the same rank.
for i=5:7
    rankByIntervals(I(i,:)==1)=i-1;
end


% Calculating Sintos summer D coeff.
[A_wedge, wedgeToEdgeList] = get_wedge_Adj(A);
weak_ind = MinStc_LogNApprox(A_wedge);
weakEdges = wedgeToEdgeList(weak_ind,:); % the weak edges
strong = ones(length(v),1);
weakEdges = [weakEdges; [weakEdges(:,2) weakEdges(:,1)]]; % accounting for undirected edges.
strong(ismember(edges,weakEdges,'rows'))=0;
somersDSintos = somersD(strong,v);

disp('--------------');
disp('# of edges in the intervals: ');
disp(num2str(numberofEdges'));
disp('--------------');
disp('Mean weight in the intervals: ');
disp(num2str(meanWeight'));
disp('--------------');
disp(['somersD intervals: ',num2str(somersD(rankByIntervals,v))]);
disp(['somersD LP2 solution: ',num2str(somersD(x1,v))]);
disp(['somersD Sintos Greedy solution: ',num2str(somersDSintos)]);
end