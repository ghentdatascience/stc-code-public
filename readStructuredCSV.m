function [Data,varnames] = readStructuredCSV(filename,hasheader,delimiter)
% Read CSV file with mixed attributes and possibly header defining variable
% names

% Init
text = strsplit(fileread(filename),{'\n','\r'});
n_lines = length(text);
n_vars = length(strsplit(text{1},delimiter));
n_entries = n_lines - hasheader;

Data = cell(n_entries,n_vars);

% Parse header
if(hasheader)
    varnames = strsplit(text{1},delimiter);
    c = 1;
else
    c = 0;
end

% Check which attributes are numeric (not very failsafe method)
numeric = false(1,n_vars);
testdata = strsplit(text{2},delimiter);
for i = 1:n_vars
    numeric(i) = strcmp(testdata{i},num2str(str2double(testdata{i})));
end

% Read file
for i = 1:n_entries
    Data(i,:) = strsplit(text{i+c},delimiter);
end

for j = find(numeric)
    for i = 1:n_entries
        Data{i,j} = str2double(Data{i,j});
    end
end

end