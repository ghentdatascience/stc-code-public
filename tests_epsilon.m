cvx_solver mosek
% cvx_solver_settings('MSK_IPAR_INTPNT_BASIS','MSK_BI_NEVER') % This setting is used in hopes that a point in the relative interior of the optimal face is found, rather than a vertex of the optimal face
% cvx_solver_settings -clear % This should be called to restore default settings, which ensure that a vertex in the optimal face is found.


%% Toy datasets

% Bow tie with connecting edge

Adj = [ones(3,3) zeros(3,3) ; zeros(3,3) ones(3,3)];
Adj(3,4)=1; Adj(4,3)=1;
Adj = Adj-diag(diag(Adj));

upper_bounded = 0;
d=1;
C=2;
force_symmetric = 0;

tic;[opt,x,epsilon,x_edges,epsilon_edges] = strong_weak_links_cvx_slacks(Adj,upper_bounded,d,C,force_symmetric);toc

figure,plot(sort(x))
figure,plot(sort(epsilon))

G = graph([x_edges(:,1) ; epsilon_edges(:,1)], [x_edges(:,2) ; epsilon_edges(:,2)], [x ; epsilon]);
LWidths = max(0,(1/d+G.Edges.Weight)+1e-5);
figure,plot(G,...'EdgeLabel',round(G.Edges.Weight*4),
    'LineWidth',LWidths);

W = 0*Adj;
for k=1:length(x_edges)
    W(x_edges(k,1),x_edges(k,2)) = x(k)+1+1/d; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x(k)+1+1/d; % +1, to distinguish weak edges from no edges
end
V = 0*Adj;
for k=1:length(epsilon_edges)
    V(epsilon_edges(k,1),epsilon_edges(k,2)) = epsilon(k)+1+1/d; % +1, to distinguish weak edges from no edges
    V(epsilon_edges(k,2),epsilon_edges(k,1)) = epsilon(k)+1+1/d; % +1, to distinguish weak edges from no edges
end
figure,subplot(1,2,1),imagesc(W),colorbar
subplot(1,2,2),imagesc(V),colorbar


% Clique with missing edge and appending edge

Adj = ones(6,6);
Adj(1,2)=0; Adj(2,1)=0;
Adj(6,7)=1; Adj(7,6)=1;
Adj = Adj-diag(diag(Adj));

upper_bounded = 0;
d=1;
C=2;
force_symmetric = 0;

tic;[opt,x,epsilon,x_edges,epsilon_edges] = strong_weak_links_cvx_slacks(Adj,upper_bounded,d,C,force_symmetric);toc

figure,plot(sort(x))
figure,plot(sort(epsilon))

G = graph([x_edges(:,1) ; epsilon_edges(:,1)], [x_edges(:,2) ; epsilon_edges(:,2)], [x ; epsilon]);
LWidths = max(0,(1/d+G.Edges.Weight)+1e-5);
figure,plot(G,...'EdgeLabel',round(G.Edges.Weight*4),
    'LineWidth',LWidths);

W = 0*Adj;
for k=1:length(x_edges)
    W(x_edges(k,1),x_edges(k,2)) = x(k)+1+1/d; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x(k)+1+1/d; % +1, to distinguish weak edges from no edges
end
V = 0*Adj;
for k=1:length(epsilon_edges)
    V(epsilon_edges(k,1),epsilon_edges(k,2)) = epsilon(k)+1+1/d; % +1, to distinguish weak edges from no edges
    V(epsilon_edges(k,2),epsilon_edges(k,1)) = epsilon(k)+1+1/d; % +1, to distinguish weak edges from no edges
end
figure,subplot(1,2,1),imagesc(W),colorbar
subplot(1,2,2),imagesc(V),colorbar


%% Karate dataset

cd 'C:\Users\tidbie\Dropbox\My Research\2017_08_31 strong and weak links'
load('karate.mat')
Adj = Problem.A;
upper_bounded = 0;
d=2;
C=2;
force_symmetric = 0;

tic;[opt,x,epsilon,x_edges,epsilon_edges] = strong_weak_links_cvx_slacks(Adj,upper_bounded,d,C,force_symmetric);toc

figure,plot(sort(x))
figure,plot(sort(epsilon))

G = graph([x_edges(:,1) ; epsilon_edges(:,1)], [x_edges(:,2) ; epsilon_edges(:,2)], [x ; epsilon]);
LWidths = max(0,(1/d+G.Edges.Weight)+1e-5);
figure,plot(G,...'EdgeLabel',round(G.Edges.Weight*4),
    'LineWidth',LWidths);

W = 0*Adj;
for k=1:length(x_edges)
    W(x_edges(k,1),x_edges(k,2)) = x(k)+1+1/d; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x(k)+1+1/d; % +1, to distinguish weak edges from no edges
end
V = 0*Adj;
for k=1:length(epsilon_edges)
    V(epsilon_edges(k,1),epsilon_edges(k,2)) = epsilon(k)+1+1/d; % +1, to distinguish weak edges from no edges
    V(epsilon_edges(k,2),epsilon_edges(k,1)) = epsilon(k)+1+1/d; % +1, to distinguish weak edges from no edges
end
figure,subplot(1,2,1),imagesc(W),colorbar
subplot(1,2,2),imagesc(V),colorbar


%% Amazon politics books

cd 'C:\Users\tidbie\Dropbox\My Research\2017_08_31 strong and weak links'
edges = gml_to_matrix('./polbooks/polbooks.gml');
edges = edges(4:end,:) + 1;
Adj = sparse(edges(:,1),edges(:,2),1,105,105);
Adj = Adj+Adj';
clear edges
upper_bounded = 0;
d=1;
C=2;
force_symmetric = 0;

tic;[opt,x,epsilon,x_edges,epsilon_edges] = strong_weak_links_cvx_slacks(Adj,upper_bounded,d,C,force_symmetric);toc

figure,plot(sort(x))
figure,plot(sort(epsilon))

G = graph([x_edges(:,1) ; epsilon_edges(:,1)], [x_edges(:,2) ; epsilon_edges(:,2)], [x ; epsilon]);
LWidths = max(0,(1/d+G.Edges.Weight)+1e-5);
figure,plot(G,...'EdgeLabel',round(G.Edges.Weight*4),
    'LineWidth',LWidths);

W = 0*Adj;
for k=1:length(x_edges)
    W(x_edges(k,1),x_edges(k,2)) = x(k)+1+1/d; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x(k)+1+1/d; % +1, to distinguish weak edges from no edges
end
V = 0*Adj;
for k=1:length(epsilon_edges)
    V(epsilon_edges(k,1),epsilon_edges(k,2)) = epsilon(k)+1+1/d; % +1, to distinguish weak edges from no edges
    V(epsilon_edges(k,2),epsilon_edges(k,1)) = epsilon(k)+1+1/d; % +1, to distinguish weak edges from no edges
end
figure,subplot(1,2,1),imagesc(W),colorbar
subplot(1,2,2),imagesc(V),colorbar



%% Les miserables

cd 'C:\Users\tidbie\Dropbox\My Research\2017_08_31 strong and weak links'
edges = gml_to_matrix('./lesmis/lesmis.gml');
edges = edges(5:end-1,:) + 1;
Adj = sparse(edges(:,1),edges(:,2),1,77,77);
Adj = Adj+Adj';
clear edges
upper_bounded = 0;
d=1;
C=2;
force_symmetric = 0;

tic;[opt,x,epsilon,x_edges,epsilon_edges] = strong_weak_links_cvx_slacks(Adj,upper_bounded,d,C,force_symmetric);toc

figure,plot(sort(x))
figure,plot(sort(epsilon))

G = graph([x_edges(:,1) ; epsilon_edges(:,1)], [x_edges(:,2) ; epsilon_edges(:,2)], [x ; epsilon]);
LWidths = max(0,(1/d+G.Edges.Weight)+1e-5);
figure,plot(G,...'EdgeLabel',round(G.Edges.Weight*4),
    'LineWidth',LWidths);

W = 0*Adj;
for k=1:length(x_edges)
    W(x_edges(k,1),x_edges(k,2)) = x(k)+1+1/d; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x(k)+1+1/d; % +1, to distinguish weak edges from no edges
end
V = 0*Adj;
for k=1:length(epsilon_edges)
    V(epsilon_edges(k,1),epsilon_edges(k,2)) = epsilon(k)+1+1/d; % +1, to distinguish weak edges from no edges
    V(epsilon_edges(k,2),epsilon_edges(k,1)) = epsilon(k)+1+1/d; % +1, to distinguish weak edges from no edges
end
figure,subplot(1,2,1),imagesc(W),colorbar
subplot(1,2,2),imagesc(V),colorbar


%% Facebook

edges = dlmread('facebook_combined.txt')+1;
Adj = sparse(edges(:,1),edges(:,2),1,max(max(edges)),max(max(edges)));
Adj = Adj+Adj';
clear edges
upper_bounded = 0;
d=1;
C=2;
force_symmetric = 0;

tic;[opt,x,epsilon,x_edges,epsilon_edges] = strong_weak_links_cvx_slacks(Adj,upper_bounded,d,C,force_symmetric);toc

figure,plot(sort(x))
figure,plot(sort(epsilon))

G = graph([x_edges(:,1) ; epsilon_edges(:,1)], [x_edges(:,2) ; epsilon_edges(:,2)], [x ; epsilon]);
LWidths = max(0,(1/d+G.Edges.Weight)+1e-5);
figure,plot(G,...'EdgeLabel',round(G.Edges.Weight*4),
    'LineWidth',LWidths);

W = 0*Adj;
for k=1:length(x_edges)
    W(x_edges(k,1),x_edges(k,2)) = x(k)+1+1/d; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x(k)+1+1/d; % +1, to distinguish weak edges from no edges
end
V = 0*Adj;
for k=1:length(epsilon_edges)
    V(epsilon_edges(k,1),epsilon_edges(k,2)) = epsilon(k)+1+1/d; % +1, to distinguish weak edges from no edges
    V(epsilon_edges(k,2),epsilon_edges(k,1)) = epsilon(k)+1+1/d; % +1, to distinguish weak edges from no edges
end
figure,subplot(1,2,1),imagesc(W),colorbar
subplot(1,2,2),imagesc(V),colorbar

