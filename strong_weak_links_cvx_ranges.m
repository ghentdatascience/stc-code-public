function [opt,x_min,x_max,x_edges] = strong_weak_links_cvx_ranges(Adj,upper_bounded,d,x_edges_requested)

% function [opt,x_min,x_max,x_edges] = strong_weak_links_cvx_ranges(Adj,upper_bounded,d,x_edges_requested)
%
% This function identifies the strength ranges for ties in a network with
% (sparse) adjacency matrix Adj.
% Inputs:
%   Adj            (the adjacency matrix)
%   upper_bounded  (equal to true iff the weights are upper bounded by 1)
%   d              (a parameter specifying what constraint is used:
%           x_ij + x_ik <= 1           for wedges, and
%           x_ij + x_ik <= 2 + d*x_jk  for triangles
%   x_edges_requested        (optional -- the edges, as pairs of vertex indices, for which the strength ranges are requested)
% Outputs:
%   opt         (optimal objective)
%   x_min       (minimum strengths under all optimal weight assignments, 0=weak, 0.5=medium, 1=strong, in simple formulation)
%   x_max       (maximum strengths under all optimal weight assignments, 0=weak, 0.5=medium, 1=strong, in simple formulation)
%   x_edges     (the edges, as pairs of vertex indices, corresponding to the x vector)

% The following outputs could easily be added:
%   ywedges     (dual variables for the wedge constraints)
%   ytriangleVs (dual variables for the triangle constraints)
%   Awedges     (The LP matrix A for wedge constraints)
%   bwedges     (The LP vector b for wedge constraints)
%   AtriangleVs (The LP matrix A for triangle constraints)
%   btriangleVs (The LP vector b for triangle constraints)

if nargin<=2
    d=1;
    if nargin==1
        upper_bounded=0;
    end
end

Adj = Adj-diag(diag(Adj)); % Just to make sure the diagonal elements are zero (no self-loops)

[is,js,~]=find(triu(Adj));
K = length(is);
N = size(Adj,1);

Adj_counter = triu(Adj);
Adj_counter(Adj_counter>0) = 1:sum(sum(Adj_counter));
Adj_counter = Adj_counter + Adj_counter';

% Enumerate all pairs of endpoints of wedges and triangleVs, and count the
% number of wedges and triangleVs

Adj2 = Adj*Adj;
Adj2 = Adj2-diag(diag(Adj2));
[wedgeends1,wedgeends2,~] = find(triu(Adj)==0 & triu(Adj2)>0);
nwedges = sum(sum(triu(Adj2).*(1-Adj)));
[triangleVends1,triangleVends2,~] = find(triu(Adj)>0 & triu(Adj2)>0);
ntriangleVs = sum(sum(triu(Adj2).*Adj));

% For each pair of endpoints, enumerate all wedges and triangleVs with these two endpoints

wedges = zeros(nwedges,2); % The wedge edges
wedgesv = zeros(nwedges,3); % The wedge vertices (middle one is the point of the wedge)

startind = 1;
for i=1:length(wedgeends1)
    endind = startind+Adj2(wedgeends1(i),wedgeends2(i))-1;
    midpoints = find(Adj(:,wedgeends1(i))>0 & Adj(:,wedgeends2(i))>0);
    wedgesv(startind:endind,1) = wedgeends1(i);
    wedgesv(startind:endind,2) = midpoints;
    wedgesv(startind:endind,3) = wedgeends2(i);
    for j=startind:endind
        wedges(j,1) = Adj_counter(wedgesv(j,1),wedgesv(j,2));
        wedges(j,2) = Adj_counter(wedgesv(j,2),wedgesv(j,3));
    end
    startind = endind+1;
end

triangleVs = zeros(ntriangleVs,3); % The triangleVs edges (first two columns) and closing edge (last column)
triangleVsv = zeros(ntriangleVs,3); % The triangleVs vertices (middle one is the point of the triangleV)

startind = 1;
for i=1:length(triangleVends1)
    endind = startind+Adj2(triangleVends1(i),triangleVends2(i))-1;
    midpoints = find(Adj(:,triangleVends1(i))>0 & Adj(:,triangleVends2(i))>0);
    triangleVsv(startind:endind,1) = triangleVends1(i);
    triangleVsv(startind:endind,2) = midpoints;
    triangleVsv(startind:endind,3) = triangleVends2(i);
    for j=startind:endind
        triangleVs(j,1) = Adj_counter(triangleVsv(j,1),triangleVsv(j,2));
        triangleVs(j,2) = Adj_counter(triangleVsv(j,2),triangleVsv(j,3));
        triangleVs(j,3) = Adj_counter(triangleVsv(j,1),triangleVsv(j,3));
    end
    startind = endind+1;
end


% Fill in values in A and b

Awedges = sparse([1:nwedges 1:nwedges]',...
    [wedges(:,1) ; wedges(:,2)],...
    ones(2*nwedges,1),...
    nwedges,K);
bwedges = ones(nwedges,1);
AtriangleVs = sparse([1:ntriangleVs 1:ntriangleVs 1:ntriangleVs]',...
    [triangleVs(:,1) ; triangleVs(:,2) ; triangleVs(:,3)],...
    [ones(2*ntriangleVs,1) ; -d*ones(ntriangleVs,1)],...
    ntriangleVs,K);
btriangleVs = 2*ones(ntriangleVs,1);


% Specify problem and run solver

cvx_begin
    variable x(K) nonnegative %integer
    dual variable ywedges
    dual variable ytriangleVs
    % maximize( x'*(1+0.01*rand(K,1)) ) % Added a small random part to break symmetries
    % maximize( sum(x) - 0.0001*x'*x ) % two-norm squared is used to break symmetry, and ensure that two medium-strength links are preferred over one weak and one strong link
    % maximize( sum(x) - 1/K*sum(abs(x-1)) ) % here the one-norm is used to break symmetry, biasing all nodes to 1
    maximize( sum(x) ) % With MOSEK also this can be used, as apparently MOSEK prefers 'integral' solutions (although not necessarily the ones with smallest norm, which is in fact a good thing if we want to maximize the number of strong links)
    subject to
        ywedges : Awedges*x <= bwedges %#ok<NOPRT>
        ytriangleVs : AtriangleVs*x <= btriangleVs %#ok<NOPRT>
        if upper_bounded
            x <= 1 %#ok<NOPRT> % This constraint can be removed if more than weak/medium/strong levels are requested (i.e. stronger levels than strong)
        end
cvx_end
opt = cvx_optval;

x_edges = [is js];
if nargin==4
    nreq = size(x_edges_requested,1);
    reqind = [];
    for i=1:nreq
        x_edge_req = sort(x_edges_requested(i,:));
        index = find(x_edges(:,1)==x_edge_req(1) & x_edges(:,2)==x_edge_req(2));
        if index
            reqind = [reqind index]; %#ok<AGROW>
        end
    end
else
    reqind = 1:size(x_edges,1);
end

% Now find the ranges for the requested edges
x_min = zeros(1,length(reqind));
x_max = zeros(1,length(reqind));

parfor i = 1:length(reqind)
    x_min(i) = minimize_edge(reqind(i), K, opt, Awedges, bwedges, AtriangleVs, btriangleVs, upper_bounded);
    x_max(i) = maximize_edge(reqind(i), K, opt, Awedges, bwedges, AtriangleVs, btriangleVs, upper_bounded);
end

x_edges = x_edges(reqind,:);