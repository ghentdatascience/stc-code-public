function [h] = plot_toy_graph(G, wedge_edges, tri_only_edges, wedges_plus, wedges_minus, tredges_plus, tredges_minus, num_tr_edges, num_w_edges, upper_lim, s_node, t_node)

    G.Nodes.XCoord = [[1:3, 1:3]*(1/3)+1, [1:3, 1:3]*(1/3)+4, ones(1, num_tr_edges*(upper_lim+1))+2, ones(1, num_tr_edges*(upper_lim+1))+5,1,7]';
    G.Nodes.YCoord = [(1:num_w_edges*2), (1:num_w_edges*2), (1:(num_tr_edges*(upper_lim+1)))+num_w_edges*2, (1:(num_tr_edges*(upper_lim+1)))+num_w_edges*2, 10,1]';
    figure(1);
    h = plot(G,'EdgeLabel', G.Edges.Weight, 'XData', G.Nodes.XCoord, 'YData', G.Nodes.YCoord);
    labelnode(h, [s_node, t_node], {'s' 't'})

    name_wp = repmat(['x'],num_w_edges,1 ) + repmat(string(wedge_edges)',1,2) + repmat(['+'],num_w_edges,1 ) +string(repmat([0, 1], num_w_edges, 1));
    name_wn = repmat(['x'],num_w_edges,1 ) + repmat(string(wedge_edges)',1,2) + repmat(['-'],num_w_edges,1 ) +string(repmat([-1, 0], num_w_edges, 1));

    name_trp = repmat(['x'],num_tr_edges,1 ) + repmat(string(full(tri_only_edges))',1,upper_lim+1) + repmat(['+'],num_tr_edges,1 ) +string(repmat(0:upper_lim, num_tr_edges, 1));
    name_trn = repmat(['x'],num_tr_edges,1 ) + repmat(string(full(tri_only_edges))',1,upper_lim+1) + repmat(['-'],num_tr_edges,1 ) +string(repmat(-upper_lim:0, num_tr_edges, 1));
    labelnode(h, wedges_plus(:)', cellstr(name_wp(:)'));
    labelnode(h, wedges_minus(:)', cellstr(name_wn(:)'));
    labelnode(h, tredges_plus(:)', cellstr(name_trp(:)'));
    labelnode(h, tredges_minus(:)', cellstr(name_trn(:)'));
    
end

