function [eq_classes,representatives, metha_nodes, mnodes_size] = get_eq_classes(triangleedges, Adj)
%%
    N = size(Adj,1);
    available = [1:size(triangleedges,1)]';
    eq_classes = {};
    k=1;
    while ~isempty(available)
        center = triangleedges(available(1),1);
        members = [center ; triangleedges(triangleedges(:,1)==center,2)];
        eq_classes{k,1} = members;
        k=k+1;
        for i=1:length(members)
            available = setdiff(available,union(find(triangleedges(:,1)==members(i)),find(triangleedges(:,2)==members(i))));
        end
    end

    n_triangle_cliques = length(eq_classes);

    eq_class_sizes = zeros(length(eq_classes),1);
    for i=1:n_triangle_cliques
        eq_class_sizes(i) = length(eq_classes{i});
    end

    remainder = setdiff([1:N]',cell2mat(eq_classes));
    for k=1:length(remainder)
        eq_classes{end+1,1} = remainder(k);
    end
    
    inverted_index = {};
    representatives = zeros(length(eq_classes), 1);
    metha_nodes = [];
    mnodes_weight = zeros(length(eq_classes), 1);
    for i=1:length(eq_classes)
        representatives(i) = eq_classes{i}(1);
        inverted_index{eq_classes{i}(1)} = i;
        if length(eq_classes{i}) > 1
            metha_nodes(end+1) = i;
            
        end
        mnodes_size(i) = length(eq_classes{i});
    end  

    %reduced_Adj = Adj(representatives, representatives);

end