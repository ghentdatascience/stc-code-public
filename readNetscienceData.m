function A = readNetscienceData()

edges = gml_to_matrix('./datasets/netscience/netscience.gml');
edges = edges + 1;
n = max(max(edges));
A = sparse(edges(:,1),edges(:,2),1,n,n);

[A,~] = computeGiantComponent(A);
end