function [opt,x_min,x_max,x_edges] = runSensitivityAnalysis(A)

CVXPATH = '\\Client\C$\Users\fawadria\matlab\cvx\';
%% Try to compute ranges
gcp('nocreate');
addpath(CVXPATH, [CVXPATH 'functions/'], [CVXPATH 'lib/']);

upper_bounded = 0;
d=4;

tic;
[opt,x_min,x_max,x_edges] = ...
    strong_weak_links_cvx_ranges(A,upper_bounded,d);
toc


end