function [v, n_match] = kendallTauC(R, G)
% Compute Kendall-tau-c 

n = length(R);
if(length(G) ~= n)
    error('R and G need to have same length.');
end

rankR = computeRanks(R);
rankG = computeRanks(G);

m = min(length(unique(rankR)),length(unique(rankG)));

n_poss = n^2 * (m-1) / m;

n_match = 0;

if(n_poss == 0)
    fprintf('%s\n','Error: Both score vectors should contain at least two unique values.')
    v = 0;
    return;
end

for i = 1:n-1
    for j = i+1:n
        switch sign(rankG(i) - rankG(j))
            case 1
                % If agree +1, if tied or reverse -1
                n_match = n_match + sign(rankR(i) - rankR(j));
            case -1
                % If agree +1, if tied or reverse -1
                n_match = n_match - sign(rankR(i) - rankR(j));
            case 0
                % Nothing
        end
    end
end

v = 2 * n_match / n_poss;

end