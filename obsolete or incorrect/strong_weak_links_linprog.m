function x = strong_weak_links_linprog(Adj)

[is,js,~]=find(triu(Adj));
K = length(is);
N = size(Adj,1);

f = -ones(K,1);
A = [];
b = [];

for k1=1:K
    e1 = is(k1);
    e2 = js(k1);
    adj1 = setdiff(find(is==e1),k1);
    adj2 = find(is==e2);
    for j=1:length(adj1)
        e3 = js(adj1(j));
        if Adj(e2,e3)
            k2 = adj1(j);
            k3 = find((is==e2 & js==e3) | (is==e3 & js==e2));
            Arow = zeros(1,K);
            Arow(1,k1) = 1;
            Arow(1,k2) = 1;
            Arow(1,k3) = -1;
            A = [A ; Arow];
            b = [b ; 1.5];
        else
            k2 = adj1(j);
            Arow = zeros(1,K);
            Arow(1,k1) = 1;
            Arow(1,k2) = 1;
            A = [A ; Arow];
            b = [b ; 1.5];
        end
    end
    for j=1:length(adj2)
        e3 = js(adj2(j));
        if Adj(e1,e3)
            k2 = adj2(j);
            k3 = find((is==e1 & js==e3) | (is==e3 & js==e1));
            Arow = zeros(1,K);
            Arow(1,k1) = 1;
            Arow(1,k2) = 1;
            Arow(1,k3) = -1;
            A = [A ; Arow];
            b = [b ; 1.5];
        else
            k2 = adj2(j);
            Arow = zeros(1,K);
            Arow(1,k1) = 1;
            Arow(1,k2) = 1;
            A = [A ; Arow];
            b = [b ; 1.5];
        end
    end
end

fperturbed = min(-0.1,f+randn(size(f))*0.01); % To break the symmetry in the problem and make the solution more integral
x = linprog(fperturbed,A,b,[],[],zeros(K,1)*4,ones(K,1));
