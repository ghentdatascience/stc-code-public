function weakEdges = MinStc_TwoApproxOld(A)
% Assuming A is the wedge graph, this will return a list of nodes (weak edges)
% as found by the 2 approximation. Based on greedily finding a maximal matching and
% selecting all nodes in that matching (edges).
[fromNode, toNode, ~] = find(A);
weakEdges = [];
while fromNode
    weakEdges = [weakEdges fromNode(1) toNode(1)];
    edge1 = fromNode(1);
    edge2 = toNode(1);
    
    % Removing all edges adjacent to edge1
    ind1=find(fromNode==edge1);
    fromNode(ind1)=[];
    toNode(ind1)=[];
    ind1=find(toNode==edge1);
    fromNode(ind1)=[];
    toNode(ind1)=[];
    
    % Removing all edges adjacent to edge2
    ind2=find(fromNode==edge2);
    fromNode(ind2)=[];
    toNode(ind2)=[];
    ind2=find(toNode==edge2);
    fromNode(ind2)=[];
    toNode(ind2)=[];   
end
end