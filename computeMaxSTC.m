function [opt,x,x_edges,epsilon,epsilon_edges] = computeMaxSTC(A,upper_bounded,d,slack,force_symmetric,C)
% Wrapper function for calling STC optimization
% Default is with C = 2, force_symmetric = true

if(nargin < 6)
    C = 2; % Default
end
if(nargin < 5)
    force_symmetric = true; % Default
end
if(nargin < 4)
    error('Not enough arguments');
end

if(slack)
    [opt,x,epsilon,x_edges,epsilon_edges] = ...
        strong_weak_links_cvx_slacks(A,upper_bounded,d,C,force_symmetric); %#ok<*UNRCH>
else
    epsilon = [];
    epsilon_edges = [];
    [opt,x,x_edges] = strong_weak_links_cvx(A,upper_bounded,d,force_symmetric);
end

end