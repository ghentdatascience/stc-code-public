function [weakEdges, margin] = MinStc_LogNApproxWithTiebreaking(A, nclosed)
% Assuming A is the wedge graph, this will return a list of nodes (weak edges)
% as found by the log n approximation. Based on greedily finding the edge
% that is part of the most wedge constraints (highest degree), and then
% removing incident edges and repeat.

% Init
A = (A | A');
margin = sum(A);
weakEdges = zeros(1,size(A,1));
c = 0;

% Run iterations
while(sum(margin) > 0)
    c = c + 1;
    % Take largest node
    v = max(margin);
    cand = find(margin == v);
    [~,cand_min_closed] = min(nclosed(cand));
    max_m = cand(cand_min_closed);
    % Find connected nodes
    q = find(A(:,max_m));
    % Update
    weakEdges(c) = max_m;
    margin(q) = margin(q) - sign(margin(q));
    margin(max_m) = 0;
end

% Take only part that was used
weakEdges = weakEdges(1:c);

end