function [opt,x,x_edges] = strong_weak_links_cvx_2var(Adj,upper_bounded,d,force_symmetric)

% function [opt,x,x_edges] = strong_weak_links_cvx_2var(Adj,upper_bounded,d,force_symmetric)
% 
% Same functionality as strong_weak_links_cvx, but now exploiting
% symmetries and yielding only a symmetric optimum

if nargin<=3
    force_symmetric=0;
    if nargin<=2
        d=2;
        if nargin==1
            upper_bounded=1;
        end
    end
end

Adj = Adj-diag(diag(Adj)); % Just to make sure the diagonal elements are zero (no self-loops)

[is,js,~]=find(triu(Adj));
K = length(is);
N = size(Adj,1);

%% Find set of edges only part of triangles

Adj1 = Adj + eye(N);
triangleedges = [];
for k=1:length(is) % Loop over all edges
    if all(Adj1(is(k),:)==Adj1(js(k),:))
        triangleedges = [triangleedges ; is(k) js(k)];
    end
end
if(~isempty(triangleedges))
    triangleedges = [triangleedges ; [triangleedges(:,2) triangleedges(:,1)]];
end

%% Find equivalence classes of points that are connected by edges only in triangles,
% and their sizes, and compute adjacency matrix

available = [1:size(triangleedges,1)]';
eq_classes = {};
k=1;
while ~isempty(available)
    center = triangleedges(available(1),1);
    members = [center ; triangleedges(triangleedges(:,1)==center,2)];
    eq_classes{k,1} = members;
    k=k+1;
    for i=1:length(members)
        available = setdiff(available,union(find(triangleedges(:,1)==members(i)),find(triangleedges(:,2)==members(i))));
    end
end

n_triangle_cliques = length(eq_classes);

eq_class_sizes = zeros(length(eq_classes),1);
for i=1:n_triangle_cliques
    eq_class_sizes(i) = length(eq_classes{i});
end

remainder = setdiff([1:N]',cell2mat(eq_classes));
for k=1:length(remainder)
    eq_classes{end+1,1} = remainder(k);
end

representatives = zeros(length(eq_classes),1);
for i=1:length(eq_classes)
    representatives(i) = eq_classes{i}(1);
end

Adj = Adj(representatives,representatives);
[is,js,~]=find(triu(Adj));
K = length(is);
N = size(Adj,1);

%% Solve problem on reduced Adj matrix

Adj_counter = triu(Adj);
Adj_counter(Adj_counter>0) = 1:sum(sum(Adj_counter));
Adj_counter = Adj_counter + Adj_counter';

% Enumerate all pairs of endpoints of wedges and triangleVs, and count the
% number of wedges and triangleVs

Adj2 = Adj*Adj;
Adj2 = Adj2-diag(diag(Adj2));
[wedgeends1,wedgeends2,~] = find(triu(Adj)==0 & triu(Adj2)>0);
nwedges = sum(sum(triu(Adj2).*(1-Adj)));

% For each pair of endpoints, enumerate all wedges and triangleVs with these two endpoints

wedges = zeros(nwedges,2); % The wedge edges
wedgesv = zeros(nwedges,3); % The wedge vertices (middle one is the point of the wedge)

startind = 1;
for i=1:length(wedgeends1)
    endind = startind+Adj2(wedgeends1(i),wedgeends2(i))-1;
    midpoints = find(Adj(:,wedgeends1(i))>0 & Adj(:,wedgeends2(i))>0);
    wedgesv(startind:endind,1) = wedgeends1(i);
    wedgesv(startind:endind,2) = midpoints;
    wedgesv(startind:endind,3) = wedgeends2(i);
    for j=startind:endind
        wedges(j,1) = Adj_counter(wedgesv(j,1),wedgesv(j,2));
        wedges(j,2) = Adj_counter(wedgesv(j,2),wedgesv(j,3));
    end
    startind = endind+1;
end

triangleVs = zeros(sum(sum(Adj(1:n_triangle_cliques,:))),2); % The first column will contain the index of the triangle_clique, the second column will contain the index of the wedge-edge
ct = 1;
for i=1:n_triangle_cliques
    for j=find(Adj(i,:))
        triangleVs(ct,:) = [i Adj_counter(i,j)];
        ct = ct+1;
    end
end
ntriangleVs = size(triangleVs,1);



% Fill in values in A and b

Awedges = sparse([[1:nwedges] [1:nwedges]]',...
    [wedges(:,1) ; wedges(:,2)],...
    ones(2*nwedges,1),...
    nwedges,K+n_triangle_cliques);
bwedges = ones(nwedges,1);
AtriangleVs = sparse([[1:ntriangleVs] [1:ntriangleVs]]',...
    [K+triangleVs(:,1) ; triangleVs(:,2)],...
    [ones(ntriangleVs,1) ; -(d-1)*ones(ntriangleVs,1)],...
    ntriangleVs,K+n_triangle_cliques);
btriangleVs = 2*ones(ntriangleVs,1);

avector = ones(K+n_triangle_cliques,1);
avector(K+1:end) = eq_class_sizes.*(eq_class_sizes-1)/2;

for i=1:n_triangle_cliques
    inds = find(Adj(i,:)==1);
    for j=inds
        if j>n_triangle_cliques
            avector(Adj_counter(i,j)) = eq_class_sizes(i);
        else
            avector(Adj_counter(i,j)) = eq_class_sizes(i)*eq_class_sizes(j);
        end
    end
end

% Specify problem and run solver

cvx_begin
    variable x(K+n_triangle_cliques) nonnegative %integer
    dual variable ywedges
    dual variable ytriangleVs
    % maximize( x'*(avector+0.01*rand(K_n_triangle_cliques,1)) ) % Added a small random part to break symmetries
    % maximize( x'*avector - 0.0001*x'*x ) % two-norm squared is used to break symmetry, and ensure that two medium-strength links are preferred over one weak and one strong link
    % maximize( x'*avector - 1/K*sum(abs(x-1)) ) % here the one-norm is used to break symmetry, biasing all nodes to 1
    maximize( x'*avector ) % With MOSEK also this can be used, as apparently MOSEK prefers 'integral' solutions (although not necessarily the ones with smallest norm, which is in fact a good thing if we want to maximize the number of strong links)
    subject to
        ywedges : Awedges*x <= bwedges
        ytriangleVs : AtriangleVs*x <= btriangleVs
        if upper_bounded
            x <= 1 % This constraint can be removed if more than weak/medium/strong levels are requested (i.e. stronger levels than strong)
        elseif d<1 % For d>=1 this constraint is redundant as well (and even incorrect for d>2)
            for kk=1:n_triangle_cliques
                if eq_class_sizes(kk)>2
                    x(K+kk) <= 2/(2-d)
                end
            end
        end
cvx_end
opt = cvx_optval;

% Now to find the symmetric solution if requested
if force_symmetric
    cvx_begin
        variable x(K+n_triangle_cliques) nonnegative %integer
        dual variable ywedges
        dual variable ytriangleVs
        minimize( (x.*x)'*avector )
        subject to
            ywedges : Awedges*x <= bwedges
            ytriangleVs : AtriangleVs*x <= btriangleVs
            x'*avector == opt;
            if upper_bounded
                x <= 1 % This constraint can be removed if more than weak/medium/strong levels are requested (i.e. stronger levels than strong)
            elseif d<1 % For d>=1 this constraint is redundant (and even incorrect for d>2)
                for kk=1:n_triangle_cliques
                    if eq_class_sizes(kk)>2
                        x(K+kk) <= 2/(2-d)
                    end
                end
            end
    cvx_end
end

%% Now map back onto original problem

xx = [];
xx_edges = [];
for k=1:n_triangle_cliques
    els = eq_classes{k};
    for i=1:length(els)
        for j=i+1:length(els)
            xx(end+1,1) = x(K+k);
            xx_edges(end+1,1:2) = [els(i) els(j)];
        end
    end
end

    for l=1:length(is)
        i_inds = eq_classes{is(l)};
        j_inds = eq_classes{js(l)};
        for i=i_inds'
            for j=j_inds'
                xx(end+1,1) = x(l);
                xx_edges(end+1,1:2) = [i j];
            end
        end
    end

x = xx;
x_edges = xx_edges;
