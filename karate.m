%% Karate dataset
load('karate.mat')
A = full(Problem.A);
c1 = [0 1 2 3 4 5 6 7 10 11 12 13 16 17 19 21]+1;
c2 = [9 14 15 18 20 22 23 24 25 26 27 28 29 30 31 32 33 8]+1;

clear Problem;

%% Try to compute ranges
gcp('nocreate');
addpath('/Users/jlijffijt/matlab/cvx/',...
    '/Users/jlijffijt/matlab/cvx/functions/', ...
    '/Users/jlijffijt/matlab/cvx/lib/');

upper_bounded = 0;
d=2;

tic;
[opt,x_min,x_max,x_edges] = ...
    strong_weak_links_cvx_ranges(A,upper_bounded,d);
toc

%% Store results
save karate_results.mat;

%% Load results
load karate_results.mat;

%% Plot both x_min and x_max as graph
plotRangeComparison(size(A,1), x_edges, x_min, x_max);