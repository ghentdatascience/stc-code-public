%% LP
d = 2;
[opt_x,x,x_edges] = strong_weak_links_cvx(A,0,2,1);

%% Hochbaum
% edges = getEdges(A);
W = STCHochbaum(edges, d);

%% Get edge values
[i,j] = find((W+0.5) > 1e-4);
h_edges = [i j];
v = zeros(length(i),1);
for k = 1:length(i)
    v(k) = W(i(k),j(k));
end
pos = v >= 0 & (h_edges(:,2) > h_edges(:,1));
h_edges = h_edges(pos,:);
v = v(pos);

clear i j pos;

[opt_x, sum(v)]