function [edges, v] = gml_to_matrix(fileName)

%Extracting edges from gml file graph
inputfile = fopen(fileName);
A = [];
v = [];
k = 1;
validedge = false;

while 1
      % Get a line from the input file
      tline = fgetl(inputfile);
      % Quit if end of file
      if ~ischar(tline)
          break
      end
      contents = strsplit(tline);
      keep = true(1,length(contents));
      for i = 1:length(contents)
          if(isempty(contents{i}))
              keep(i) = false;
          end
      end
      contents = contents(keep);
      switch contents{1}
          case 'source'
              A(k,1) = str2num(contents{2});
          case 'target'
              A(k,2) = str2num(contents{2});
              validedge = true;
          case 'value'
              if(validedge)
                  v(k,1) = str2num(contents{2});
              end
          case ']'
              if(validedge)
                k = k + 1;
                validedge = false;
              end
      end
end
edges = A;
