%% Read data
[A, edges, v] = readGoTData();

%% Test rank of symmetric solution against edge weights
d = 2;
C = 2;

% Compute symmetric optimal solution
[~,x,x_edges] = strong_weak_links_cvx(A,0,d,1);
x = round(x*2)/2;

[~,xs,~,xs_edges,~] = strong_weak_links_cvx_slacks(A,0,d,C,1);
xs = round(xs*2)/2;

% Give index of edges to map to x_edges
ix = alignEdges(edges,x_edges);
ix2 = alignEdges(edges,xs_edges);

%% Compute Kendall-tau rank correlation
[kt, n_poss] = kendallTauAsymmetric(x,v(ix));
[kts, n_poss_s] = kendallTauAsymmetric(xs,v(ix2));

%% Visualize comparison
x2 = x;
x2(x2 >= 1.0) = 1;
boxplot(v(ix),x2);
xlabel('Edge strength at optimum');
ylabel('Number of interactions');
ylim([0 20]);
title('Edge strengths vs interactions, Game of Thrones data');
set(gca,'XTickLabel',{'Weak','Unknown','Strong'});


%% Try to compute ranges
gcp('nocreate');
addpath('/Users/jlijffijt/matlab/cvx/',...
    '/Users/jlijffijt/matlab/cvx/functions/', ...
    '/Users/jlijffijt/matlab/cvx/lib/');

upper_bounded = 0;
d=1;

tic;
[opt,x_min,x_max,x_edges] = ...
    strong_weak_links_cvx_ranges(A,upper_bounded,d);
toc

%% Store results
save got_results.mat;

%% Load results
load got_results.mat;

%% Plot both x_min and x_max as graph
plotRangeComparison(size(A,1), x_edges, x_min, x_max)