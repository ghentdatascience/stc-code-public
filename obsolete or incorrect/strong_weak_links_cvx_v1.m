function x = strong_weak_links_cvx(Adj)

[is,js,~]=find(triu(Adj));
K = length(is);
N = size(Adj,1);

cvx_begin
    variable x(K) nonnegative
    % maximize( x'*(1+0.01*rand(K,1)) ) % Added a small random part to break symmetries
    maximize( sum(x) - 0.01*x'*x ) % two-norm squared is used to break symmetry
    increment = 10^(max(0,round(log10(K))-3));
    for k1=1:K
        if floor(k1/increment)==k1/increment
            disp([num2str(k1) ' of ' num2str(K)])
        end
        e1 = is(k1);
        e2 = js(k1);
        adj1 = setdiff(find(is==e1),k1);
        adj2 = find(is==e2);
        for j=1:length(adj1)
            e3 = js(adj1(j));
            if Adj(e2,e3)
                k2 = adj1(j);
                k3 = find((is==e2 & js==e3) | (is==e3 & js==e2));
                x(k1)+x(k2)-x(k3) <= 1.5
            else
                k2 = adj1(j);
                x(k1)+x(k2) <= 1.5
            end
        end
        for j=1:length(adj2)
            e3 = js(adj2(j));
            if Adj(e1,e3)
                k2 = adj2(j);
                k3 = find((is==e1 & js==e3) | (is==e3 & js==e1));
                x(k1)+x(k2)-x(k3) <= 1.5
            else
                k2 = adj2(j);
                x(k1)+x(k2) <= 1.5
            end
        end
    end
    x <= 1
cvx_end
