function [wedge_Adj, edgeList] = get_wedge_Adj(A)
% Compute the wedge graph Adj. matrix, given (undirected) adj. matrix A.
% the ith row in wedge_Adj refers to the edge edgeList(i).

A = A - diag(diag(A));
[n,m] = size(A);
if(m ~= n)
    error('Adjacency matrix must be square');
end

if(~all(A == (A|A')))
    error('Adjacency matrix must be symmetric');
end

[r, c, ~] = find(A - tril(A,-1)); 
edgeList = sortrows([r c]); % the indexing of the edges in the wedge graph

% Finding all wedges
% First finding the endpoints of wedges.
Adj2 = A*A;
Adj2 = Adj2-diag(diag(Adj2));
[wedgeends1,wedgeends2,~] = find(triu(A)==0 & triu(Adj2)>0);
nwedges = sum(sum(triu(Adj2).*(1-A)));

% Finding the midpoints for each pair of endpoints.
wedgesv = zeros(nwedges,3);
startind = 1;
    for i=1:length(wedgeends1)
        endind = startind+Adj2(wedgeends1(i),wedgeends2(i))-1;
        midpoints = find(A(:,wedgeends1(i))>0 & A(:,wedgeends2(i))>0);
        wedgesv(startind:endind,1) = wedgeends1(i);
        wedgesv(startind:endind,2) = midpoints;
        wedgesv(startind:endind,3) = wedgeends2(i);
        startind = endind+1;
    end
[~,conn1a] = ismember([wedgesv(:,1) wedgesv(:,2)],edgeList,'rows');    
[~,conn1b] = ismember([wedgesv(:,2) wedgesv(:,1)],edgeList,'rows');
conn1=conn1a+conn1b;

[~,conn2a] = ismember([wedgesv(:,2) wedgesv(:,3)],edgeList,'rows');    
[~,conn2b] = ismember([wedgesv(:,3) wedgesv(:,2)],edgeList,'rows');
conn2=conn2a+conn2b;

wedge_Adj = sparse(conn1, conn2, 1, length(edgeList), length(edgeList));
wedge_Adj = wedge_Adj|wedge_Adj';
end