function weakEdges = MinStc_LogNApproxOld(A)
% Assuming A is the wedge graph, this will return a list of nodes (weak edges)
% as found by the log n approximation. Based on greedily finding the edge
% that is part of the most wedge constraints (highest degree), and then
% removing incident edges and repeat.

[fromNode, toNode, ~] = find(A);
weakEdges = [];
while fromNode
    maxi = mode(fromNode);
    weakEdges = [weakEdges maxi];
    % Removing all edges adjacent to maxi
    ind=find(fromNode==maxi);
    fromNode(ind)=[];
    toNode(ind)=[];
    ind=find(toNode==maxi);
    fromNode(ind)=[];
    toNode(ind)=[];
end

end