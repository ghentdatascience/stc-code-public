function [triangleedges] = get_triangles(Adj)
   
    [is,js,~]=find(triu(Adj));
    K = length(is);
    N = size(Adj,1);

    %% Find set of edges only part of triangles

    Adj1 = Adj + eye(N);
    triangleedges = [];
    for k=1:length(is) % Loop over all edges
        if all(Adj1(is(k),:)==Adj1(js(k),:))
            triangleedges = [triangleedges ; is(k) js(k)];
        end
    end
    if(~isempty(triangleedges))
        triangleedges = [triangleedges ; [triangleedges(:,2) triangleedges(:,1)]];
    end
end

