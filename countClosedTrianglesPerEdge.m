function nclosed = countClosedTrianglesPerEdge(A)
% Compute number of closed triangles that each edge participates in

[n,m] = size(A);
[a,b] = find(A);

if(m ~= n)
    error('Adjacency matrix must be square');
end

A = A | A';

ntri = sparse(a,b,1);

for i = 1:n
    connected_i = find(A(:,i));
    for j = 1:length(connected_i)
        connected_j = setdiff(find(A(:,connected_i(j))),i);
        for k = 1:length(connected_j)
            if(A(i,connected_j(k)))
                ntri(i,connected_i(j)) = full(ntri(i,connected_i(j))) + 1; %#ok<*SPRIX>
            end
        end
    end
end

for i = 1:length(a)
    if(a(i) > b(i))
        q = (full(ntri(a(i),b(i))) + full(ntri(b(i),a(i))) - 2)/2;
        ntri(a(i),b(i)) = q;
        ntri(b(i),a(i)) = q;
    end
end

[a,b] = find(triu(A));
nclosed = zeros(1,length(a));
for i = 1:length(a)
    nclosed(i) = full(ntri(a(i),b(i)));
end

end