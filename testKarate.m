function h = testKarate()

load('karate.mat')
A = full(Problem.A);
c1 = [0 1 2 3 4 5 6 7 10 11 12 13 16 17 19 21]+1;
c2 = [9 14 15 18 20 22 23 24 25 26 27 28 29 30 31 32 33 8]+1;
clear Problem;

%[~,x,x_edges] = strong_weak_links_cvx_2var(A,0,1,1);
[~,x,epsilon,x_edges,epsilon_edges] = strong_weak_links_cvx_slacks(A,0,1,2/3,0);
interE = [];
intraE = [];
for i=1:nnz(A)/2 
    if (ismember(x_edges(i,1),c1) && ismember(x_edges(i,2),c2)) || (ismember(x_edges(i,1),c2) && ismember(x_edges(i,2),c1))
        interE = [interE; [x_edges(i,1) x_edges(i,2)]];
    else
        intraE = [intraE; [x_edges(i,1) x_edges(i,2)]];
    end
end

[~,ind_inter]=ismember(interE,x_edges,'rows');
[~,ind_intra]=ismember(intraE,x_edges,'rows');

find(epsilon>-0.999)

end