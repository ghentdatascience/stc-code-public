%% Read data
[A, edges, v] = readLMData();

%% Somers' D analysis
h = runSomersDAnalysis(A, edges, v);

%% Range analysis
tic;
[opt,x_min,x_max,x_edges] = runSensitivityAnalysis(A);
toc

%% Store results
save miserables_results.mat;

%% Load results
load miserables_results.mat;

%% Plot both x_min and x_max as graph
plotRangeComparison(size(A,1), x_edges, x_min, x_max)

%% Test equivalence of Hochbaum solution
% Compute Hochbaum solution
W = STCHochbaum(edges, d);
drx
%% Get edge values
[i,j] = find((W+0.5) > 0);
h_edges = [i j];
v = zeros(length(i),1);
for k = 1:length(i)
    v(k) = W(i(k),j(k));
end
pos = v >= 0 & (h_edges(:,2) > h_edges(:,1));
h_edges = h_edges(pos,:);
v = v(pos);

clear i j pos;