%% Read data
A = readPBData();

%% Try to compute ranges
gcp('nocreate');
addpath('/Users/jlijffijt/matlab/cvx/',...
    '/Users/jlijffijt/matlab/cvx/functions/', ...
    '/Users/jlijffijt/matlab/cvx/lib/');

upper_bounded = 0;
d=2;

tic;
[opt,x_min,x_max,x_edges] = ...
    strong_weak_links_cvx_ranges(A,upper_bounded,d);
toc

%% Store results
clear ans;

save polbooks_results.mat;

%% Load results
load polbooks_results.mat;

%% Plot both x_min and x_max as graph
plotRangeComparison(size(A,1), x_edges, x_min, x_max)