function x = strong_weak_links_cvx_v2(Adj)

[is,js,~]=find(triu(Adj));
K = length(is);
N = size(Adj,1);

increment = 10^(max(0,round(log10(K))-2));

% Count how many constraints there are, so we can initialize A and b

ncons=0;
for k1=1:K
    if floor(k1/increment)==k1/increment
        disp([num2str(k1) ' of ' num2str(K)])
    end
    e1 = is(k1);
    e2 = js(k1);
    num_adj1 = sum(is==e1)-1;
    num_adj2 = sum(is==e2);
    ncons = ncons+num_adj1+num_adj2;
end
A = sparse(ncons,K,3*ncons);
b = zeros(ncons,1);

% Fill in values in A and b

row = 1;
for k1=1:K
    if floor(k1/increment)==k1/increment
        disp([num2str(k1) ' of ' num2str(K)])
    end
    e1 = is(k1);
    e2 = js(k1);
    adj1 = setdiff(find(is==e1),k1);
    adj2 = find(is==e2);
    for j=1:length(adj1)
        e3 = js(adj1(j));
        if Adj(e2,e3)
            k2 = adj1(j);
            k3 = find((is==e2 & js==e3) | (is==e3 & js==e2));
            A(row,k1) = 1;
            A(row,k2) = 1;
            A(row,k3) = -1;
            b(row) = 1.5;
        else
            k2 = adj1(j);
            A(row,k1) = 1;
            A(row,k2) = 1;
            b(row) = 1.5;
        end
        row = row+1;
    end
    for j=1:length(adj2)
        e3 = js(adj2(j));
        if Adj(e1,e3)
            k2 = adj2(j);
            k3 = find((is==e1 & js==e3) | (is==e3 & js==e1));
            A(row,k1) = 1;
            A(row,k2) = 1;
            A(row,k3) = -1;
            b(row) = 1.5;
        else
            k2 = adj2(j);
            A(row,k1) = 1;
            A(row,k2) = 1;
            b(row) = 1.5;
        end
        row = row+1;
    end
end

% Specify problem and run solver

cvx_begin
    variable x(K) nonnegative
    % maximize( x'*(1+0.01*rand(K,1)) ) % Added a small random part to break symmetries
    maximize( sum(x) - 0.01*x'*x ) % two-norm squared is used to break symmetry
    subject to
        A*x <= b
        x <= 1
cvx_end
