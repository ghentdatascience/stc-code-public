function [x,y,Es,A] = strong_weak_links_cvx_integer(Adj)

% function [x,y,Es,A] = strong_weak_links_cvx_integer(Adj)
%
% This function identifies strong, weak, and medium ties in a network with
% (sparse) adjacency matrix Adj.
% Outputs:
%   x  (weights, 0 for weak, 1 for medium, and 2 for strong)
%   y  (dual variables for the constraints)
%   Es (sets of three vertices involved in each of the constraints, in the same order as the dual variables y -- the V edges are between the first and second, and between the second and third vertex listed in each row of Es)
%   A  (The LP matrix A)


[is,js,~]=find(triu(Adj));
K = length(is);
N = size(Adj,1);

Adj_counter = triu(Adj);
Adj_counter(Adj_counter>0) = 1:sum(sum(Adj_counter));
Adj_counter = Adj_counter + Adj_counter';

increment = 10^(max(0,round(log10(K))-2));

% Count how many constraints there are, so we can initialize A and b

ncons=0;
for k12=1:K
    if floor(k12/increment)==k12/increment
        disp([num2str(k12) ' of ' num2str(K)])
    end
    e1 = is(k12);
    e2 = js(k12);
    num_adj1 = sum(Adj(e1,1:e2-1));
    num_adj2 = sum(Adj(e2,e1+1:end));
    ncons = ncons+num_adj1+num_adj2;
end
Ar = zeros(3*ncons,1);
Ac = zeros(3*ncons,1);
Av = zeros(3*ncons,1);

% Fill in values in A and b

ct = 1;
row = 1;
Es = zeros(3*ncons,3);
for k12=1:K
    if floor(k12/increment)==k12/increment
        disp([num2str(k12) ' of ' num2str(K)])
    end
    e1 = is(k12);
    e2 = js(k12);
    adj1 = find(Adj(e1,1:e2-1));
    adj2 = e1+find(Adj(e2,e1+1:end));
    for j=1:length(adj1)
        e0 = adj1(j);
        k01 = Adj_counter(e0,e1);
        Ac(ct) = k01;
        Ar(ct) = row;
        Av(ct) = 1;
        ct = ct+1;
        Ac(ct) = k12;
        Ar(ct) = row;
        Av(ct) = 1;
        ct = ct+1;
        if Adj(e0,e2)
            k02 = Adj_counter(e0,e2);
            Ac(ct) = k02;
            Ar(ct) = row;
            Av(ct) = -1;
            ct = ct+1;
        end
        Es(row,:) = [e0 e1 e2];
        row = row+1;
    end
    for j=1:length(adj2)
        e3 = adj2(j);
        k23 = Adj_counter(e2,e3);
        Ac(ct) = k12;
        Ar(ct) = row;
        Av(ct) = 1;
        ct = ct+1;
        Ac(ct) = k23;
        Ar(ct) = row;
        Av(ct) = 1;
        ct = ct+1;
        if Adj(e1,e3)
            k13 = Adj_counter(e1,e3);
            Ac(ct) = k12;
            Ar(ct) = row;
            Av(ct) = -1;
            ct = ct+1;
        end
        Es(row,:) = [e1 e2 e3];
        row = row+1;
    end
end
Arcv = [Ar(1:ct-1) Ac(1:ct-1) Av(1:ct-1)];
Es = Es(1:row-1,:);
A = sparse(Arcv(:,1),Arcv(:,2),Arcv(:,3),max(Arcv(:,1)),K);
b = 1.5*ones(size(A,1),1);

% Specify problem and run solver

cvx_begin
    variable x(K) nonnegative %integer
    dual variable y
    % maximize( x'*(1+0.01*rand(K,1)) ) % Added a small random part to break symmetries
    % maximize( sum(x) - 0.0001*x'*x ) % two-norm squared is used to break symmetry, and ensure that two medium-strength links are preferred over one weak and one strong link
    % maximize( sum(x) - 1/K*sum(abs(x-1)) ) % here the one-norm is used to break symmetry, biasing all nodes to 1
    maximize( sum(x) ) % With MOSEK also this can be used, as apparently MOSEK prefers 'integral' solutions (although not necessarily the ones with smallest norm, which is in fact a good thing if we want to maximize the number of strong links)
    subject to
        y : A*(x+2) <= 4*b
        x <= 2 % This constraint can be removed if more than weak/medium/strong levels are requested (i.e. stronger levels than strong)
cvx_end
