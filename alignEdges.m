function ix = alignEdges(edges1,edges2)
% Compute index ix of edges1 to map to edges2, i.e., ix(1) containts the
% how-manieth edge of edges1 is the first edge of edges2

% Init
edges1 = sort(edges1,2);
edges2 = sort(edges2,2);
n = size(edges1,1);
ix = zeros(1,n);
if(n ~= size(edges2,1))
    error('Edge sets do not match');
end

% Create map for edges2
ixset = cell(1,max(edges2(:,2)));
for k = 1:n
    j = edges2(k,2);
    ixset{j} = horzcat(ixset{j}, k);
end

% Match edges
for k = 1:n
    e = edges1(k,:);
    z = ixset{e(2)};
    subix = find(edges2(z,1) == e(1));
    if(length(subix) == 1)
        ix(k) = z(subix);
    else
        error('Edge sets do not match.');
    end
end

end