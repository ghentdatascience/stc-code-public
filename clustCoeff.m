function [cc, nopp, ntri] = clustCoeff(A)
% Compute clustering coefficient for symmetric adjacency matrix A
% CODE ASSUMES THERE ARE NO SELF-EDGES (EMPTY DIAGONAL)

[n,m] = size(A);
if(m ~= n)
    error('Adjacency matrix must be square');
end

nopp = 0;
ntri = 0;

for i = 1:n
    connected_i = find(A(i,:));
    for j = 1:length(connected_i)
        connected_j = setdiff(find(A(connected_i(j),:)),i);
        % Wedges incident to j
        for k = 1:length(connected_j)
            nopp = nopp + 1;
            if(A(i,connected_j(k)))
                ntri = ntri + 1;
            end
        end
        % Wedges incident to i
        
    end
end

ntri = ntri / 6;
nopp = nopp / 2;

cc = 3*ntri/nopp;

end