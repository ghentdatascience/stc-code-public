function sd = computeSomersDSeriesSlack_ExtraStrongLinks(A, edges, v, C, MaxStrongLinks)

% CVXPATH = '\\Client\C$\Users\fawadria\matlab\cvx\';
CVXPATH = '/Users/jlijffijt/matlab/cvx/';
gcp('nocreate');
% addpath(CVXPATH, [CVXPATH 'functions\'], [CVXPATH 'lib\']);
addpath(CVXPATH, [CVXPATH 'functions/'], [CVXPATH 'lib/']);
% cvx_solver mosek

% Init
d = 1;
n_s = length(MaxStrongLinks);
n_C = length(C);
sd = zeros(n_C,n_s);

for i = 1:n_C
    for j = 1:n_s
        % Compute symmetric optimal solution
        [~,x,~,x_edges,~] = strong_weak_links_cvx_slacks_ExtraBound(A,0,d,C(i),MaxStrongLinks(j),0);
        x = round(x*2)/2;

        % Give index of edges to map to x_edges
        ix = alignEdges(edges,x_edges);
        
        % Compute Somers' D rank correlation
        sd(i,j)  = kendallTauExact(x(ix),v);
    end
end

end