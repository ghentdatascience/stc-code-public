cvx_solver mosek
% cvx_solver_settings('MSK_IPAR_INTPNT_BASIS','MSK_BI_NEVER') % This setting is used in hopes that a point in the relative interior of the optimal face is found, rather than a vertex of the optimal face
% cvx_solver_settings -clear % This should be called to restore default settings, which ensure that a vertex in the optimal face is found.


%% Toy datasets

% Clique with missing edge and appending edge

Adj = ones(6,6);
Adj(1,2)=0; Adj(2,1)=0;
Adj(6,7)=1; Adj(7,6)=1;
Adj = Adj-diag(diag(Adj));

[is,js,~] = find(triu(Adj));
G = graph(is,js,ones(length(is),1));
figure,plot(G);

upper_bounded = 0;
d=1/2;
force_symmetric = 1;

tic;[opt,x_min,x_max,x_edges] = strong_weak_links_cvx_ranges(Adj,upper_bounded,d);toc

[x_edges x_min' x_max']



% Bow tie with connecting edge

Adj = [ones(3,3) zeros(3,3) ; zeros(3,3) ones(3,3)];
Adj(3,4)=1; Adj(4,3)=1;
Adj = Adj-diag(diag(Adj));

[is,js,~] = find(triu(Adj));
G = graph(is,js,ones(length(is),1));
figure,plot(G);

upper_bounded = 0;
d=1/8;
force_symmetric = 0;

tic;[opt,x_min,x_max,x_edges] = strong_weak_links_cvx_ranges(Adj,upper_bounded,d);toc

[x_edges x_min' x_max']


% Counterexample to symmetricity of optimum

Adj = zeros(16,16);
Adj(1:3,1:3)=1;
Adj(4,1:3)=1; Adj(1:3,4)=1;
Adj(5,1:3)=1; Adj(1:3,5)=1;
Adj(6,1:3)=1; Adj(1:3,6)=1;
Adj(7:16,6)=1; Adj(6,7:16)=1;
Adj(7:16,7:16)=1;
Adj = Adj-diag(diag(Adj));

[is,js,~] = find(triu(Adj));
G = graph(is,js,ones(length(is),1));
figure,plot(G);

upper_bounded = 0;
d=2;
force_symmetric = 0;

tic;[opt,x_min,x_max,x_edges] = strong_weak_links_cvx_ranges(Adj,upper_bounded,d);toc

[x_edges x_min' x_max']


%% Karate dataset

%cd 'C:\Users\tidbie\Dropbox\My Research\2017_08_31 strong and weak links'
load('karate.mat')
Adj = Problem.A;
upper_bounded = 0;
d=1;

tic;[opt,x_min,x_max,x_edges] = strong_weak_links_cvx_ranges(Adj,upper_bounded,d);toc

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_min(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_min(k)+1; % +1, to distinguish weak edges from no edges
end
figure,subplot(1,3,1),imagesc(W)

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_max(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_max(k)+1; % +1, to distinguish weak edges from no edges
end
subplot(1,3,2),imagesc(W)

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_max(k)-x_min(k); % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_max(k)-x_min(k); % +1, to distinguish weak edges from no edges
end
subplot(1,3,3),imagesc(W)



%% Amazon politics books

cd 'C:\Users\tidbie\Dropbox\My Research\2017_08_31 strong and weak links'
edges = gml_to_matrix('./polbooks/polbooks.gml');
edges = edges(4:end,:) + 1;
Adj = sparse(edges(:,1),edges(:,2),1,105,105);
Adj = Adj+Adj';
clear edges
upper_bounded = 0;
d=1;

tic;[opt,x_min,x_max,x_edges] = strong_weak_links_cvx_ranges(Adj,upper_bounded,d);toc

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_min(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_min(k)+1; % +1, to distinguish weak edges from no edges
end
figure,subplot(1,3,1),imagesc(W)

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_max(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_max(k)+1; % +1, to distinguish weak edges from no edges
end
subplot(1,3,2),imagesc(W)

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_max(k)-x_min(k); % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_max(k)-x_min(k); % +1, to distinguish weak edges from no edges
end
subplot(1,3,3),imagesc(W)


%% Les miserables

cd 'C:\Users\tidbie\Dropbox\My Research\2017_08_31 strong and weak links'
edges = gml_to_matrix('./lesmis/lesmis.gml');
edges = edges(5:end-1,:) + 1;
Adj = sparse(edges(:,1),edges(:,2),1,77,77);
Adj = Adj+Adj';
clear edges
upper_bounded = 0;
d=5/3;

tic;[opt,x_min,x_max,x_edges] = strong_weak_links_cvx_ranges(Adj,upper_bounded,d);toc

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_min(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_min(k)+1; % +1, to distinguish weak edges from no edges
end
figure,subplot(1,3,1),imagesc(W)

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_max(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_max(k)+1; % +1, to distinguish weak edges from no edges
end
subplot(1,3,2),imagesc(W)

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_max(k)-x_min(k); % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_max(k)-x_min(k); % +1, to distinguish weak edges from no edges
end
subplot(1,3,3),imagesc(W)



%% Facebook

edges = dlmread('facebook_combined.txt')+1;
Adj = sparse(edges(:,1),edges(:,2),1,max(max(edges)),max(max(edges)));
Adj = Adj+Adj';
clear edges
upper_bounded = 0;
d=1;

tic;[opt,x,x_edges] = strong_weak_links_cvx_ranges(Adj,upper_bounded,d);toc

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_min(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_min(k)+1; % +1, to distinguish weak edges from no edges
end
figure,subplot(1,3,1),imagesc(W)

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_max(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_max(k)+1; % +1, to distinguish weak edges from no edges
end
subplot(1,3,2),imagesc(W)

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_max(k)-x_min(k); % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_max(k)-x_min(k); % +1, to distinguish weak edges from no edges
end
subplot(1,3,3),imagesc(W)


%%

%cd 'C:\Users\tidbie\Dropbox\My Research\2017_08_31 strong and weak links'
%M = dlmread('./datasets/students.csv', ';');
M = dlmread('./datasets/enron.csv', ';');
%M = dlmread('./datasets/facebookBig.csv', ';');
%M = dlmread('./datasets/twitterBig.csv', ';');
%M = dlmread('./datasets/tumblr.csv', ';');
%M = dlmread('enron.csv', ';');
edges = M(:,[1 2]);
Adj = sparse(edges(:,1),edges(:,2),1,max(max(edges)),max(max(edges)));
Adj = Adj+Adj';
clear edges
upper_bounded = 0;
d=2;

tic;[opt,x,x_edges] = strong_weak_links_cvx_ranges(Adj,upper_bounded,d);toc

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_min(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_min(k)+1; % +1, to distinguish weak edges from no edges
end
figure,subplot(1,3,1),imagesc(W)

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_max(k)+1; % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_max(k)+1; % +1, to distinguish weak edges from no edges
end
subplot(1,3,2),imagesc(W)

W = zeros(size(Adj));
for k=1:size(x_edges,1)
    W(x_edges(k,1),x_edges(k,2)) = x_max(k)-x_min(k); % +1, to distinguish weak edges from no edges
    W(x_edges(k,2),x_edges(k,1)) = x_max(k)-x_min(k); % +1, to distinguish weak edges from no edges
end
subplot(1,3,3),imagesc(W)
