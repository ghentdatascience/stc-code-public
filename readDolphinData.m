function A = readDolphinData()

edges = gml_to_matrix('./datasets/dolphins/dolphins.gml');
edges = edges + 1;
n = max(max(edges));
A = sparse(edges(:,1),edges(:,2),1,n,n);
A = full(A | A');

end