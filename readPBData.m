function A = readPBData()

edges = gml_to_matrix('./datasets/polbooks/polbooks.gml');
edges = edges + 1;
A = sparse(edges(:,1),edges(:,2),1,105,105);
A = full(A | A');

end