function W = computeAdjMatrix(edges, values)
% Construct adjacency matrix W of all zeroes with values on edges

% Init
n_nodes = max(max(edges));
n_edges = size(edges,1);

if(nargin < 2)
    values = ones(1,size(edges,1));
end


W = zeros(n_nodes);

for i = 1:n_edges
    W(edges(i,1), edges(i,2)) = values(i);
    W(edges(i,2), edges(i,1)) = values(i);
end

end