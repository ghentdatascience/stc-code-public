function h = runLinkPrediction(A, nit, C)
% C can be a vector of different values
% nit indiciates for each C value how many times we delete/add random edges
cvx_solver mosek
numberofC = length(C);
k = nnz(A)/2/100; % adding a small fraction of the total edges.
k = round(k);

P1vsC = zeros(1,numberofC);
R1vsC = zeros(1,numberofC);
P2vsC = zeros(1,numberofC);
R2vsC = zeros(1,numberofC);

n_r = zeros(1,nit);
n_a = zeros(1,nit);

for l = 1:length(C)
    P_1 = NaN(1,nit);
    R_1 = NaN(1,nit);
    P_2 = NaN(1,nit);
    R_2 = NaN(1,nit);
        for n = 1:nit
        A2 = A;
        % Adding k random edges to the graph.
        [i, j] = find(triu(~A2,1));
        poolofE = [i,j];
        k_ind = randperm(length(i),k); % k random edges without repetitions.
        addedEdges = poolofE(k_ind,:);

        % Removing k random edges to the graph.
        [i, j] = find(triu(A2,1));
        poolofE = [i,j];
        k_ind = randperm(length(i),k);
        removedEdges = poolofE(k_ind,:);

        A2(addedEdges(:,1), addedEdges(:,2))=1;
        A2(addedEdges(:,2), addedEdges(:,1))=1;
        A2(removedEdges(:,1), removedEdges(:,2))=0;
        A2(removedEdges(:,2), removedEdges(:,1))=0;

        [~,x,epsilon,x_edges,epsilon_edges] = strong_weak_links_cvx_slacks(A2,0,1,C(l),0);
        x = round(2*x)/2;
        epsilon = round(2*epsilon)/2;

        % Precision and Recall for the removal of added edges.
        suggestRemovedEdges = x_edges(x==-1,:);
        n_suggestr = length(suggestRemovedEdges);
        n_r(n) = n_suggestr;
        n_added = length(addedEdges);
        n_both = nnz(ismember(addedEdges,suggestRemovedEdges,'rows'));
        P_1(n) = n_both/n_suggestr;
        R_1(n) = n_both/n_added;

        % Precision and Recall for the addition of deleted edges.
        suggestAddedEdges = epsilon_edges(epsilon>-1,:);
        n_suggesta = length(suggestAddedEdges);
        n_a(n) = n_suggesta;
        n_removed = length(removedEdges);
        n_both = nnz(ismember(removedEdges,suggestAddedEdges,'rows'));
        P_2(n) = n_both/n_suggesta;
        R_2(n) = n_both/n_removed;
        end

    ind = ~isnan(P_1);
    if length(find(ind))>10 % Problem if no edge is suggested for deletion (P=NaN), we neglect these cases while still having a sign. set
        P_1=P_1(ind);
        R_1=R_1(ind);
    else
        P_1 = NaN(1,nit);
        R_1 = NaN(1,nit);
    end
    ind = ~isnan(P_2);
    if length(find(ind))>10 % Idem, but for addition.
        P_2 = P_2(ind);
        R_2 = R_2(ind);
     else
        P_2 = NaN(1,nit);
        R_2 = NaN(1,nit);
    end
    P1vsC(l) = mean(P_1);
    R1vsC(l) = mean(R_1);
    P2vsC(l) = mean(P_2);
    R2vsC(l) = mean(R_2);
end
figure
plot(C, P1vsC,'--','LineWidth',1.8,'Color',[1 0 0])
hold on
plot(C, R1vsC,'LineWidth',1.8,'Color',[1 0 0])
hold on
plot(C, P2vsC,'--','LineWidth',1.8,'Color',[0 1 0])
hold on
plot(C, R2vsC,'LineWidth',1.8,'Color',[0 1 0])
title('P/R LP4 for simultaneous random edge addition/deletion on Les Mis.')
xlabel('C')
xticks(C)
yticks([0 0.1 0.2 0.3 0.4 0.5 0.6])
ylabel('Precision and Recall scores [0-1]')
legend('P for suggested removal', 'R for suggested removal', 'P for suggested addition', 'R for suggested addition')
hold off
set(gca,'fontsize',13); % this can be changed later.
end
