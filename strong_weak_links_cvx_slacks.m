function [opt,x,epsilon,x_edges,epsilon_edges] = strong_weak_links_cvx_slacks(Adj,upper_bounded,d,C,force_symmetric)

% function [opt,x,epsilon,x_edges,epsilon_edges] = strong_weak_links_cvx_slacks(Adj,upper_bounded,d,C,force_symmetric)
%
% This function identifies strong, weak, and medium ties in a network with
% (sparse) adjacency matrix Adj, but allows edges to be -1 ('absent') and
% a slack per wedge.
% Inputs:
%   Adj           (the adjacency matrix)
%   upper_bounded (equal to true iff the weights are upper bounded by d)
%   d             (a parameter specifying what constraint is used:
%           x_ij + x_ik <= 2 + d*x_jk  (for ij and ik edges)
%           x_jk >= -1/d               (for jk end-points of a path of length 2)
%   C             (the weight for the sum of the epsilons in the objective)
%   force_symmetric (1 if the solution must be invariant under graph automorphisms)
% Outputs:
%   opt            (optimal value of the objective)
%   x              (an optimal weight assignment, -1/d=no edge, 0=weak, 0.5=medium, 1=strong, in simple formulation)
%   epsilon        (an optimal assignment of the epsilon vector, -1/d corresponds to no slack)
%   x_edges        (the edges, as pairs of vertex indices, corresponding to the x vector)
%   epsilon_edges  (the pairs of vertex indices corresponding to the epsilon vector)

if nargin<=4
    force_symmetric = 0;
    if nargin<=3
        C = 2;
        if nargin<=2
            d=1;
            if nargin==1
                upper_bounded=0;
            end
        end
    end
end

Adj = Adj-diag(diag(Adj)); % Just to make sure the diagonal elements are zero (no self-loops)

[is,js,~]=find(triu(Adj));
K = length(is);
N = size(Adj,1);

Adj_counter = triu(Adj);
Adj_counter(Adj_counter>0) = 1:sum(sum(Adj_counter));
Adj_counter = Adj_counter + Adj_counter';

% Enumerate all pairs of endpoints of wedges and triangleVs, and count the
% number of wedges and triangleVs

Adj2 = Adj*Adj;
Adj2 = Adj2-diag(diag(Adj2));
[wedgeends1,wedgeends2,~] = find(triu(Adj)==0 & triu(Adj2)>0);
nwedges = sum(sum(triu(Adj2).*(1-Adj)));
[triangleVends1,triangleVends2,~] = find(triu(Adj)>0 & triu(Adj2)>0);
ntriangleVs = sum(sum(triu(Adj2).*Adj));

% For each pair of endpoints, enumerate all wedges and triangleVs with these two endpoints

wedges = zeros(nwedges,2); % The wedge edges
wedgesv = zeros(nwedges,3); % The wedge vertices (middle one is the point of the wedge)
epsilonedges = zeros(nwedges,1); % An index for the epsilon edges into the epsilon variable vector

startind = 1;
for i=1:length(wedgeends1)
    endind = startind+Adj2(wedgeends1(i),wedgeends2(i))-1;
    midpoints = find(Adj(:,wedgeends1(i))>0 & Adj(:,wedgeends2(i))>0);
    wedgesv(startind:endind,1) = wedgeends1(i);
    wedgesv(startind:endind,2) = midpoints;
    wedgesv(startind:endind,3) = wedgeends2(i);
    for j=startind:endind
        wedges(j,1) = Adj_counter(wedgesv(j,1),wedgesv(j,2));
        wedges(j,2) = Adj_counter(wedgesv(j,2),wedgesv(j,3));
    end
    epsilonedges(startind:endind) = i;
    startind = endind+1;
end

triangleVs = zeros(ntriangleVs,3); % The triangleVs edges (first two columns) and closing edge (last column)
triangleVsv = zeros(ntriangleVs,3); % The triangleVs vertices (middle one is the point of the triangleV)

startind = 1;
for i=1:length(triangleVends1)
    endind = startind+Adj2(triangleVends1(i),triangleVends2(i))-1;
    midpoints = find(Adj(:,triangleVends1(i))>0 & Adj(:,triangleVends2(i))>0);
    triangleVsv(startind:endind,1) = triangleVends1(i);
    triangleVsv(startind:endind,2) = midpoints;
    triangleVsv(startind:endind,3) = triangleVends2(i);
    for j=startind:endind
        triangleVs(j,1) = Adj_counter(triangleVsv(j,1),triangleVsv(j,2));
        triangleVs(j,2) = Adj_counter(triangleVsv(j,2),triangleVsv(j,3));
        triangleVs(j,3) = Adj_counter(triangleVsv(j,1),triangleVsv(j,3));
    end
    startind = endind+1;
end


% Fill in values in A and b

Awedges = sparse([[1:nwedges] [1:nwedges]]',...
    [wedges(:,1) ; wedges(:,2)],...
    ones(2*nwedges,1),...
    nwedges,K);
Aslacks = sparse([1:nwedges]', epsilonedges, d*ones(nwedges,1), nwedges, length(wedgeends1));
bwedges = 2*ones(nwedges,1);
AtriangleVs = sparse([[1:ntriangleVs] [1:ntriangleVs] [1:ntriangleVs]]',...
    [triangleVs(:,1) ; triangleVs(:,2) ; triangleVs(:,3)],...
    [ones(2*ntriangleVs,1) ; -d*ones(ntriangleVs,1)],...
    ntriangleVs,K);
btriangleVs = 2*ones(ntriangleVs,1);


% Specify problem and run solver

cvx_begin
    variable x(K)
    variable epsilon(length(wedgeends1))
    maximize( sum(x) - C*sum(epsilon))
    subject to
        x >= -1/d %instead of 0 here would make sense for symmetry between the x's en epsilons, but it does not seem to work well in practice (after very limited testing)
        epsilon >= -1/d
        Awedges*x <= bwedges + Aslacks*epsilon
        AtriangleVs*x <= btriangleVs
        if upper_bounded
            x <= 1 % This constraint can be removed if more than weak/medium/strong levels are requested (i.e. stronger levels than strong)
        end
cvx_end
opt = cvx_optval;

epsilon_edges = [wedgeends1 wedgeends2];
x_edges = [is js];

% Now find symmetric optimum if requested
if force_symmetric
    cvx_begin
        variable x(K)
        variable epsilon(length(wedgeends1))
        minimize( x'*x + epsilon'*epsilon)
        subject to
            sum(x) - C*sum(epsilon) == opt
            x >= -1/d %instead of 0 here would make sense for symmetry between the x's en epsilons, but it does not seem to work well in practice (after very limited testing)
            epsilon >= -1/d
            Awedges*x <= bwedges + Aslacks*epsilon
            AtriangleVs*x <= btriangleVs
            if upper_bounded
                x <= 1 % This constraint can be removed if more than weak/medium/strong levels are requested (i.e. stronger levels than strong)
            end
    cvx_end
end
