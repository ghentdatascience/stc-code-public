function [A, edges, v] = readDBLP()

fileID = fopen('./datasets/dblp_graph_number_of_papers.csv', 'r');
M = fscanf(fileID,'%d %d %d', [3 inf]);
fclose(fileID);
edges = [M(1,:); M(2,:)]';
n = max(max(edges));
A = sparse(edges(:,1),edges(:,2),1,n,n);
v = M(3,:)';
A = full(A | A');
end