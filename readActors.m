function [A,edges,v] = readActors()
M = dlmread('./datasets/graphAllActors.txt','\t');
edges_orig = [M(:,1) M(:,2)];
v_orig = M(:,3);

% Rearranging edges_orig and v_orig so it contains only 1 weight per edgepair.
[edges_orig, iv, ~] = unique(sort(edges_orig,2),'rows'); % only contains upper triangular edges.
v_orig = v_orig(iv);

n = max(max(edges_orig));
A_orig = sparse(edges_orig(:,1),edges_orig(:,2),1,n,n);

% Largest connected component.
[A,gcc] = computeGiantComponent(A_orig); 

% Computing the edgelist from the giant component adj. matrix
[i,j] = find(triu(A>0)); % only consider uppertriangular edges.
edges = [i j];

% Finding the v-values of the edges in the giant component.
[~,edge_ix] = ismember([gcc(i) gcc(j)], edges_orig, 'rows');
v = v_orig(edge_ix);
end