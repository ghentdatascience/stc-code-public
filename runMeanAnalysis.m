function h = runMeanAnalysis(A, edges, v)
% Computing the mean weight for the different types of edges

% Sintos log n algo. (2 types of edges: s/w)
[A_wedge, wedgeToEdgeList] = get_wedge_Adj(A);
weak_ind = MinStc_LogNApprox(A_wedge);
weakEdges = wedgeToEdgeList(weak_ind,:); % the weak edges
strong = ones(length(v),1);
weakEdges = [weakEdges; [weakEdges(:,2) weakEdges(:,1)]]; % accounting for undirected edges.
strong(ismember(edges,weakEdges,'rows'))=0;

n_strong = sum(strong>0);
n_weak = length(strong)-n_strong;
weight_s_Santos1 = sum(v(strong>0))/n_strong;
weight_w_Santos1 = sum(v(strong==0))/n_weak;

% Sintos 2 factor algo. (2 types of edges: s/w)
weak_ind = MinStc_TwoApprox(A_wedge);
weakEdges = wedgeToEdgeList(weak_ind,:); % the weak edges
strong = ones(length(v),1);
weakEdges = [weakEdges; [weakEdges(:,2) weakEdges(:,1)]]; % accounting for undirected edges.
strong(ismember(edges,weakEdges,'rows'))=0;

n_strong = sum(strong>0);
n_weak = length(v)-n_strong;
weight_s_Santos2 = sum(v(strong>0))/n_strong;
weight_w_Santos2 = sum(v(strong==0))/n_weak;

% LP1-STC
d = 1;
[~,x,x_edges] = strong_weak_links_cvx_2var(A,1,d,1);
x = round(x*2)/2;
ix = alignEdges(edges,x_edges);
x = x(ix); % aligning the x-values.

n_weak_lp1 = sum(x==0);
n_medium_lp1 = sum(x==1/2);
n_strong_lp1 = sum(x==1);

weight_w_LP1 = sum(v(x==0))/n_weak_lp1;
weight_m_LP1 = sum(v(x==1/2))/n_medium_lp1;
weight_s_LP1 = sum(v(x==1))/n_strong_lp1;

% LP2-STC
d = 1;
[~,x,x_edges] = strong_weak_links_cvx_2var(A,0,d,1);
x = round(x*2)/2;
ix = alignEdges(edges,x_edges);
x = x(ix); % aligning the x-values.

n_weak_lp2 = sum(x==0);
n_medium1_lp2 = sum(x==1/2);
n_medium2_lp2 = sum(x==1);
n_strong_lp2 = sum(x==2);

weight_w_LP2 = sum(v(x==0))/n_weak_lp2;
weight_m1_LP2 = sum(v(x==1/2))/n_medium1_lp2;
weight_m2_LP2 = sum(v(x==1))/n_medium2_lp2;
weight_s_LP2 = sum(v(x==2))/n_strong_lp2;

%{
Range analysis LP2-STC.
[~,x_min,x_max,x_edges] = runSensitivityAnalysis(A);
x_min = round(x_min*2)/2;
x_max = round(x_max*2)/2;
ix = alignEdges(edges,x_edges);
x_min=x_min(ix);
x_max=x_max(ix);
[u,~,~]=unique([x_min' x_max'],'rows');
for i=1:length(u)
    occ = ismember([x_min' x_max'],u(i,:),'rows');
    n_occ = nnz(occ);
    disp(['Interval: [', num2str(u(i,1)),' ',num2str(u(i,2)),']'])
    disp(['Occurences: ', num2str(n_occ), ' -- Mean weight: ', num2str(sum(v(occ))/n_occ)]);
end
%}

disp('Santos Greedy')
disp([num2str(n_strong),' 1-edges, and ',num2str(n_strong),' 0-edges.'])
disp(['Strong: ',num2str(weight_s_Santos1)]);
disp(['Weak: ',num2str(weight_w_Santos1)]);
disp('-------------')
%{
disp('Santos MaximalMatching')
disp(['Strong: ',num2str(weight_s_Santos2)]);
disp(['Weak: ',num2str(weight_w_Santos2)]);
disp('-------------')
%}
disp('LP1')
disp([num2str(n_strong_lp1),' 1-edges, ',num2str(n_medium_lp1),' (1/2)-edges and ',num2str(n_weak_lp1),' 0-edges.'])
disp(['Strong: ',num2str(weight_s_LP1)]);
disp(['Medium: ',num2str(weight_m_LP1)]);
disp(['Weak: ',num2str(weight_w_LP1)]);
disp('-------------')
disp('LP2')
disp([num2str(n_strong_lp2),' 2-edges, ',num2str(n_medium2_lp2),' 1/-edges, ',num2str(n_medium1_lp2),' (1/2)-edges and ',num2str(n_weak_lp2),' 0-edges'])
disp(['Strong: ',num2str(weight_s_LP2)]);
disp(['Medium2: ',num2str(weight_m2_LP2)]);
disp(['Medium1: ',num2str(weight_m1_LP2)]);
disp(['Weak: ',num2str(weight_w_LP2)]);

end