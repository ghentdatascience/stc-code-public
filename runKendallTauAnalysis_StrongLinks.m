function h = runKendallTauAnalysis_StrongLinks(A, edges, v)

% cvx_solver mosek

% Init
d = 1;
C = 1:4;
strong = 0:2:80;

kts = computeSomersDSeriesSlack_ExtraStrongLinks(A,edges,v,C,strong);

% Visualize
figure;
% Slack
hold on
for i = 1:length(C)
    plot(strong,kts(i,:),'o-');
    legendlabels{i} = ['C = ' num2str(C(i))];
end

ylabel('Kendall tau');
xlabel('MaxStrong');
legend(legendlabels);

end