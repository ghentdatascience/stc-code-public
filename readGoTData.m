function [A, edges, w] = readGoTData()

%% Read data
[Data,varnames] = readStructuredCSV('datasets/got/gotedgesseason1.csv',1,';');

%% Create hashmap of characters
charmap = containers.Map('KeyType','char','ValueType','double');
charmap_inv = containers.Map('KeyType','double','ValueType','char');

nedges = size(Data,1);
nchars = 0;

for i = 1:nedges
    if(~isKey(charmap,Data{i,1}))
        nchars = nchars + 1;
        charmap(Data{i,1}) = nchars;
        charmap_inv(nchars) = Data{i,1};
    end
    if(~isKey(charmap,Data{i,2}))
        nchars = nchars + 1;
        charmap(Data{i,2}) = nchars;
        charmap_inv(nchars) = Data{i,2};
    end
end

clear i;

%% Create edge list
edges = zeros(nedges,2);
w = zeros(nedges,1);

for i = 1:nedges
    edges(i,1) = charmap(Data{i,1});
    edges(i,2) = charmap(Data{i,2});
    w(i) = Data{i,6};
end

clear i;

%% Create graph
A = sparse([edges(:,1) edges(:,2)],[edges(:,2) edges(:,1)],1,nchars,nchars);
A = full(A);

end